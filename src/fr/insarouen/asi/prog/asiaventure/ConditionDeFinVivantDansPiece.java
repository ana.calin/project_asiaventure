package fr.insarouen.asi.prog.asiaventure;

/**
* Condition de fin conjonction ConditionDeFin
* Classe ayant le but de faire la conjonction des ConditionDeFin
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
import fr.insarouen.asi.prog.asiaventure.ConditionDeFin;
import fr.insarouen.asi.prog.asiaventure.EtatDuJeu;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;

public class ConditionDeFinVivantDansPiece extends ConditionDeFin{

  // Attributs

  private EtatDuJeu etatDuJeu;
  private Vivant vivant;
  private Piece piece;

  // Constructeurs

  /**
  * Création d'une ConditionDeFinVivantDansPiece
  * @param etatConditionVerifiee l'etat du jeu
  * @param vivant le Vivant
  * @param piece la Piece
  */
  public ConditionDeFinVivantDansPiece(EtatDuJeu etatConditionVerifiee,Vivant vivant,Piece piece){
      super(etatConditionVerifiee);
      this.vivant=vivant;
      this.piece=piece;

   }

   // Méthodes

   /**
   * Verification d'etat du jeu
   * @return EtatDuJeu
   */
   public EtatDuJeu verifierCondition(){

         if(this.piece.contientVivant(this.vivant)){
           return getEtatConditionVerifiee();
         }
         else return EtatDuJeu.ENCOURS;
   }
}
