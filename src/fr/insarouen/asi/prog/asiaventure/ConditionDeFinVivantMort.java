package fr.insarouen.asi.prog.asiaventure;

/**
* Condition de fin conjonction ConditionDeFinVivantMort
* Classe ayant le but de redefinir la condition de fin en fonction du status du vivant
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
import fr.insarouen.asi.prog.asiaventure.ConditionDeFin;
import fr.insarouen.asi.prog.asiaventure.EtatDuJeu;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;

public class ConditionDeFinVivantMort extends ConditionDeFin {

  // Attributs

  private EtatDuJeu etatDuJeu;
  private Vivant vivant;

  // Constructeurs

  /**
  * Création d'une ConditionDeFinVivantMort
  * @param etatConditionVerifiee l'etat du jeu
  * @param vivant le Vivant
  */
  public ConditionDeFinVivantMort(EtatDuJeu etatConditionVerifiee,Vivant vivant){
    super(etatConditionVerifiee);
    this.vivant=vivant;
  }

  // Méthodes

  /**
  * Verification d'etat du jeu
  * @return EtatDuJeu
  */
  public EtatDuJeu verifierCondition(){
     if(this.vivant.estMort()){
        return getEtatConditionVerifiee();
     }
     else return EtatDuJeu.ENCOURS;
  }

}
