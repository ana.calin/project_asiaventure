package fr.insarouen.asi.prog.asiaventure;

import fr.insarouen.asi.prog.asiaventure.ConditionDeFin;
import fr.insarouen.asi.prog.asiaventure.EtatDuJeu;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
import java.util.*;

/**
* Condition de fin conjonction ConditionDeFin
* Classe ayant le but de faire la conjonction des ConditionDeFin
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ConditionDeFinConjonctionConditionDeFin extends ConditionDeFin {

  // Attributs
  
  private ConditionDeFin[] conditions;

  // Constructeurs

  /**
  * Création d'une ConditionDeFinConjonctionConditionDeFin
  * @param etatDuJeu l'etat du jeu
  * @param cfs le(s) condition(s) de fin
  */
  public ConditionDeFinConjonctionConditionDeFin(EtatDuJeu etatDuJeu, ConditionDeFin... cfs){
     super(etatDuJeu);
     this.conditions=cfs;
  }

  // Méthodes

  /**
  * Verification d'etat du jeu
  * @return EtatDuJeu
  */
  public EtatDuJeu verifierCondition(){
     for(ConditionDeFin c : this.conditions){
       if(c.verifierCondition() == EtatDuJeu.ENCOURS){
          return EtatDuJeu.ENCOURS;
       }
     }
     return this.getEtatConditionVerifiee();
  }

}
