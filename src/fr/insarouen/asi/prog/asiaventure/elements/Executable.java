package fr.insarouen.asi.prog.asiaventure.elements;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;

/**
* Création de l'interface Executable
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/

public interface Executable {

	// Méthodes
	
	/**
	* Permet d'executer
	*/
	public void executer() throws Throwable; // il n'y avait pas public dans la javadoc
}
