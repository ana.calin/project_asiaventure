package fr.insarouen.asi.prog.asiaventure.elements;

/**
* Etat est de type énuméré
* Enumeration des différents etats
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public enum Etat {CASSE, DEVERROUILLE, FERME, OUVERT, VERROUILLE};
