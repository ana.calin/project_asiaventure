package fr.insarouen.asi.prog.asiaventure.elements;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;

/**
* Création de la classe ActivationImpossibleAvecObjetException fille de ActivationException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ActivationImpossibleAvecObjetException extends ActivationException {

  // Constructeurs

  /**
  * Création d'un ActivationImpossibleAvecObjetException
  */
  public ActivationImpossibleAvecObjetException() {
  }

  /**
  * Création d'un ActivationImpossibleAvecObjetException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ActivationImpossibleAvecObjetException(String msg){
    super(msg);
  }
}
