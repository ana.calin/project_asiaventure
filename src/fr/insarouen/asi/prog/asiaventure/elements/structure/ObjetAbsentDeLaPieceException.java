package fr.insarouen.asi.prog.asiaventure.elements.structure;

/**
* Création de la classe ObjetAbsentDeLaPieceException fille de PieceException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ObjetAbsentDeLaPieceException extends PieceException {

  // Constructeurs

  /**
  * Création de ObjetAbsentDeLaPieceException
  */
  public ObjetAbsentDeLaPieceException() {
  }

  /**
  * Création de ObjetAbsentDeLaPieceException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ObjetAbsentDeLaPieceException(String msg) {
    super(msg);
  }



}
