package fr.insarouen.asi.prog.asiaventure.elements.structure;

/**
* Exception lancée lorsque l'on essaye de franchir une porte qui ne se trouve pas dans la pièce.
*/

public class PorteInexistanteDansLaPieceException extends PieceException {

  // Constructeurs

  /**
  * Création de PorteInexistanteDansLaPieceException
  */
  public PorteInexistanteDansLaPieceException() {
  }

  /**
  * Création de PorteInexistanteDansLaPieceException
  * @param msg le message de l'exception.
  */
  public PorteInexistanteDansLaPieceException(String msg) {
    super(msg);
  }
}
