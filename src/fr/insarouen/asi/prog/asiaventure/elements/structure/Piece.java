package fr.insarouen.asi.prog.asiaventure.elements.structure;

import fr.insarouen.asi.prog.asiaventure.elements.structure.ElementStructurel;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Porte;
import java.util.*;

/**
* Création de la classe Piece fille de ElementStructurel
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class Piece extends ElementStructurel {

  // Attributs

  /**
  * Collection d'Entite dans ce Monde
  */
  private HashMap<String, Objet> mapObjet;

  /**
  * Collection contenant l'ensemble des Vivants présents dans la Piece.
  */
  private HashMap<String, Vivant> mapVivant;

  private HashMap<String, Porte> mapPorte;

  // Constructeur

  /**
  * Création d'une Piece
  * @param nom nom de la Piece
  * @param monde le monde
  */
  public Piece(String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde);
    this.mapObjet= new HashMap<String, Objet>();
    this.mapVivant = new HashMap<String, Vivant>();
    this.mapPorte = new HashMap<String, Porte>();
  }

  // Méthodes

  /**
  * Permet de déposer un Objet dans la Piece
  * @param obj un objet
  */
  public void deposer(Objet obj) {
    this.mapObjet.put(obj.getNom(), obj);
  }

  /**
  * Permet de retirer un objet d'une Piece
  * @param obj un objet
  * @return Objet l'objet qu'on retire
  */
  public Objet retirer(Objet obj) throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
    retirer(obj.getNom());

    return obj;
  }

  /**
  * Permet de retirer un Objet d'une Piece à partir de son nom
  * @param nomObj nom de l'Objet
  * @return Objet l'Objet qu'on retire (null si l'Objet n'est pas présent dans la Piece)
  */
  public Objet retirer(String nomObj) throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
    Objet objetAretirer;

    if (!(this.mapObjet.containsKey(nomObj))) {
      throw new ObjetAbsentDeLaPieceException("Vous ne pouvez pas retirer l'Objet "+nomObj+" de la Pièce car il n'est pas présent dans la Pièce");
    }

    if (!(this.mapObjet.get(nomObj).estDeplacable())) {
      throw new ObjetNonDeplacableException("Vous ne pouvez pas retirer l'Objet "+nomObj+" de la Pièce car il n'est pas déplaçable");
    }

    objetAretirer=this.mapObjet.get(nomObj);
    this.mapObjet.remove(nomObj);

    return objetAretirer;
  }

  /**
  * Permet de savoir si un Objet est présent dans une Piece
  * @param obj un objet
  * @return boolean
  */
  public boolean contientObjet(Objet obj) {
    return contientObjet(obj.getNom());
  }

  /**
  * Permet de savoir si un objet est présent dans une Piece à partir du nom de l'Objet
  * @param nomObj nom de l'Objet
  * @return boolean
  */
  public boolean contientObjet(String nomObj) {
    return this.mapObjet.containsKey(nomObj);
  }

  /**
  * Permet d'ajouter une porte à la Piece
  * @param porte la Porte à ajouter
  */
	public void addPorte(Porte porte) { //protected
  	mapPorte.put(porte.getNom(), porte);
  }

  /**
  * Permet de savoir si une porte est dans la pièce.
  * @param porte la porte à tester
  * @return boolean true si la porte est dans la pièce, false sinon
  */
  public boolean aLaPorte(Porte porte) {
  	return aLaPorte(porte.getNom());
  }

  /**
  * Permet de savoir si une porte est dans la Piece
  * @param nomPorte le nom de la Porte à tester
  * @return boolean true si la Porte est dans la Piece, false sinon
  */
  public boolean aLaPorte(String nomPorte) {
  	return mapPorte.containsKey(nomPorte);
  }

  /**
  * Permet d'obtenir la Porte de la Piece à partir de son nom
  * @param nomPorte le nom de la porte à trouver dans la pièce
  * @return Porte la porte identifiée par ce nom. Retourne null si la porte n'est pas trouvée
  */
  public Porte getPorte(String nomPorte) {
  	return mapPorte.get((Object)nomPorte);
  }

  /**
  * Permet de décrire l'objet Piece
  * @return String
  */
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(super.toString()).append("\n");;

    sb.append("Objet(s) dans la Piece : ").append("\n");
    for (Objet o : this.mapObjet.values()) {
        sb.append(o.getNom()).append("\n");
    }

    sb.append("Vivants : ").append("\n");
    for (Vivant v : this.mapVivant.values()) {
      sb.append(v.toString()).append("\n");
    }

    sb.append("Porte : ");
    for (Porte p : this.mapPorte.values()) {
      sb.append(p.getNom()).append(", ");
    }
    sb.append("\n");

    return sb.toString();
  }

  /**
  * Permet d'ajouter un Vivant dans la Piece.
  * @param vivant Vivant a ajouter
  */
  public void entrer(Vivant vivant) {
      this.mapVivant.put(vivant.getNom(), vivant);
  }

    /**
  * Permet de faire sortir un Vivant de la Piece
  * @param vivant Vivant à sortir
  * @return Vivant Vivant à sorir
  */
  public Vivant sortir(Vivant vivant) throws VivantAbsentDeLaPieceException {
    sortir(vivant.getNom());

    return vivant;
  }

  /**
  * Permet de faire sortir un Vivant de la Piece à partir de son nom
  * @param nomVivant nom du Vivant à sortir
  * @return Vivant Vivant à sorir (null si le Vivant n'est pas présent dans la Piece)
  */
  public Vivant sortir(String nomVivant) throws VivantAbsentDeLaPieceException {
    Vivant vivantASortir;
    if (!(contientVivant(nomVivant))) {
      throw new VivantAbsentDeLaPieceException("Vous ne pouvez pas faire sortir le Vivant "+nomVivant+" car il n'est pas présent dans la Pièce");
    }

    vivantASortir=this.mapVivant.get(nomVivant);

    this.mapVivant.remove(nomVivant);
    return vivantASortir;
  }

  /**
  * Permet de savoir si un Vivant est présent dans la pièce
  * @param vivant le Vivant
  * @return boolean
  */
  public boolean contientVivant(Vivant vivant) {
    return contientVivant(vivant.getNom());
  }

  /**
  * Permet de savoir si un Vivant est présent dans la pièce à partir de son nom
  * @param nomVivant le Vivant
  * @return boolean
  */
  public boolean contientVivant(String nomVivant) {
    return this.mapVivant.containsKey(nomVivant);
  }

  // Méthodes Damien
  /**
  * Permet d'obtenir un Objet présent dans la pièce à partir de son nom
  * @param nomObj nom de l'Objet
  * @return Objet l'Objet voulu (null si l'Objet n'est pas présent dans la Piece)
  */
  public Objet getObjet (String nomObj) {
    return this.mapObjet.get(nomObj);
  }

  /**
  * Permet d'obtenir tous les Objets présents dans la pièce
  * @return Collection<Objet> les Objets de la Piece
  */
  public Collection<Objet> getObjets() {
    return this.mapObjet.values();
  }

  public Collection<Porte> getPortes(){
    return this.mapPorte.values();
  }

}
