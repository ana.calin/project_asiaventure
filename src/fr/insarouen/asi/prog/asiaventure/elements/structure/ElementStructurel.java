package fr.insarouen.asi.prog.asiaventure.elements.structure;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

/**
* Création de la classe abstraire ElementStructurel fille de Entite
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public abstract class ElementStructurel extends Entite {

  /**
  * Création d'un ElementStructurel
  * @param nom nom
  * @param monde monde
  */
  public ElementStructurel(String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde);
  }

  /**
  * Permet de transformer le nom et le monde de l'ElementStructurel en string
  * @return String
  */
  public String toString() {
    return super.toString();
  }
}
