package fr.insarouen.asi.prog.asiaventure.elements.structure;

import fr.insarouen.asi.prog.asiaventure.elements.Activable;
import fr.insarouen.asi.prog.asiaventure.elements.structure.ElementStructurel;
import fr.insarouen.asi.prog.asiaventure.elements.Etat;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Serrure;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleAvecObjetException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationException;

/**
* Création de la classe Porte fille de ElementStructurel et qui implémente Activable
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class Porte extends ElementStructurel implements Activable {

  // Attributs

  private Etat etat = Etat.OUVERT;
  private Piece pieceA;
  private Piece pieceB;
  private Serrure serrure = null;

  // Constructeurs

  /**
  * Création d'une Porte
  * @param nom nom de la Porte
  * @param monde le Monde
  * @param pieceA la PieceA
  * @param pieceB la PieceB
  */
  public Porte(String nom, Monde monde, Piece pieceA, Piece pieceB) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde);
    this.pieceA = pieceA;
    this.pieceB = pieceB;

    this.pieceA.addPorte(this);
		this.pieceB.addPorte(this);

  }

  /**
  * Création d'une Porte
  * @param nom nom de la Porte
  * @param monde le Monde
  * @param serrure la PieceB
  * @param pieceA la PieceA
  * @param pieceB la PieceB
  */
  public Porte(String nom, Monde monde, Serrure serrure, Piece pieceA, Piece pieceB) throws NomDEntiteDejaUtiliseDansLeMondeException {
    this(nom, monde,  pieceA, pieceB);
    this.serrure = serrure;
  }

  // Méthodes

  /**
  * Permet d'activer la Porte (OUVERT ou FERME)
  */
  public void activer() throws ActivationImpossibleException {
    Etat etatPorte=getEtat();

    if (etatPorte == Etat.CASSE) {
			throw new ActivationImpossibleException("La Porte ne peut plus être activée car elle est CASSEE");
		}

    switch (etatPorte) {
           case OUVERT :
                    this.etat = Etat.FERME;
                    break;
           case FERME :
                    this.etat = Etat.OUVERT;
                    break;
    }
  }

  /**
  * Permet d'activer une porte à l'aide d'un objet.
  * @param obj Objet permettant d'activer la porte
  */
  public void activerAvec(Objet obj) throws ActivationImpossibleAvecObjetException, ActivationImpossibleException {
    Etat etatPorte=getEtat();

    if ((this.serrure==null) && (obj instanceof Clef)) {
      throw new ActivationImpossibleException("L'activation de la Porte avec une Clef est impossible car la Porte ne possède pas de Serrure");
    }

    if (!(this.activableAvec(obj))) {
			throw new ActivationImpossibleAvecObjetException("Vous ne pouvez pas activer la Porte car l'Objet utilisé n'est pas un PiedDeBiche");
		}

		if (obj instanceof PiedDeBiche) {
      if (etatPorte == Etat.VERROUILLE) {
        this.etat = Etat.CASSE;
      }
      else {
        if (etatPorte == Etat.FERME) {
          this.etat = Etat.CASSE;
        }
      }
			return;
		}

    // A partir d'ici on aura forcément une Clef
    switch (etatPorte) {
      case FERME :
        this.etat = Etat.VERROUILLE;
        return;
      case VERROUILLE :
        this.etat = Etat.OUVERT;
        return;
      case OUVERT :
        this.etat = Etat.VERROUILLE;
        return;
    }
  }

  /**
  * Permet de savoir si une Porte est activable à partir d'un Objet donne
  * @param obj Objet à tester
  */
  public boolean activableAvec(Objet obj) {
	   return ((obj instanceof PiedDeBiche) || ((this.serrure != null) && (this.serrure.activableAvec(obj))));
  }

  /**
  * Permet d'obtenir l'Etat la Porte
  * @return Etat l'etat de la porte
  */
  public Etat getEtat() {
    return this.etat;
  }

  /**
  * Permet d'obtenir la Serrure de la Porte
  * @return Serrure la Serrure de la porte
  */
  public Serrure getSerrure() {
    return this.serrure;
  }

  /**
  * Permet d'obtenir la Piece voisine
  * @param piece nom de la Piece courante
  * @return Piece la Piece voisine
  */
  public Piece getPieceAutreCote(Piece piece) {
    if (piece.equals(this.pieceA)) {
      return this.pieceB;
    }
    else {
      if (piece.equals(this.pieceB)) {
        return this.pieceA;
      }
    }

    return null;
  }

  /**
  * Permet de décrire l'objet Porte
  * @return String
  */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append("La porte est ").append(getEtat()).append(" entre les Pieces : ").append("\n");
    sb.append(this.pieceA.toString()).append("\n");
    sb.append(this.pieceB.toString());

    return sb.toString();
  }
}
