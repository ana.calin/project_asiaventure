package fr.insarouen.asi.prog.asiaventure.elements.structure;

import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import  fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.VivantAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;

/**
* Création de la classe ClassMainTestPiece
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ClassMainTestPiece {

  /**
  * Main pour ClassMainTestPiece
  * @param args les parametres qui peuvent etre entres via l'entree standard
  */
  public static void main(String[] args) {

    try {
    Monde m1 = new Monde("toto");
    Piece piece = new Piece("cuisine", m1);
    PiedDeBiche piedDeBiche1 = new PiedDeBiche("crowbar1", m1);
    PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", m1);
    PiedDeBiche piedDeBiche3 = new PiedDeBiche("crowbar3", m1);

// Tout ce qui concerne Objet

    // Test deposer, contientObjet(Objet), contientObjet(nomObjet)
    piece.deposer(piedDeBiche1);
    piece.deposer(piedDeBiche2);
    piece.deposer(piedDeBiche3);

    if (piece.contientObjet(piedDeBiche3)) {
      System.out.println("deposer(Objet) et piece.contientObjet(Objet) OK");
    }
    else {
      System.out.println("deposer(Objet) et piece.contientObjet(Objet) KO");
    }

    if (piece.contientObjet("crowbar3")) {
      System.out.println("deposer(Objet) et piece.contientObjet(nomObjet) OK");
    }
    else {
      System.out.println("deposer(Objet) et piece.contientObjet(nomObjet) KO");
    }

    // Test retirer(nomObjet)
    piece.retirer("crowbar3");
    if (!(piece.contientObjet(piedDeBiche3.getNom()))) {
      System.out.println("retirer(nomObjet) OK");
    }
    else {
      System.out.println("retirer(nomObjet) KO");
    }

    // Test retirer(Objet)
    piece.retirer(piedDeBiche2);
    if (!(piece.contientObjet(piedDeBiche2))) {
      System.out.println("retirer(Objet) OK");
    }
    else {
      System.out.println("retirer(Objet) KO");
    }

// Tout ce qui concerne Vivant
    int pointVie = 10;
    int pointForce = 10;
    PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[3];

    piece.deposer(piedDeBiche2);
    piece.deposer(piedDeBiche3);

    tabPiedDeBiche[0]=piedDeBiche1;
    tabPiedDeBiche[1]=piedDeBiche2;
    tabPiedDeBiche[2]=piedDeBiche3;

    Vivant vivant1 = new Vivant("homme", m1, pointVie, pointForce, piece, tabPiedDeBiche){};
    Vivant vivant2 = new Vivant("homme2", m1, pointVie, pointForce, piece, tabPiedDeBiche){};

    // Test entrer(nomVivant)
    piece.entrer(vivant1);
    if (piece.contientVivant(vivant1.getNom())) {
      System.out.println("contientVivant(nomVivant) vivant1 OK");
    }
    else {
      System.out.println("contientVivant(nomVivant) vivant1 KO");
    }

    if (!(piece.contientVivant(vivant2.getNom()))) {
      System.out.println("contientVivant(nomVivant) pas vivant2 OK");
    }
    else {
      System.out.println("contientVivant(nomVivant) pas vivant2 KO");
    }

    // Test entrer(Vivant)
    piece.entrer(vivant2);
    if ((piece.contientVivant(vivant1)) && (piece.contientVivant(vivant2))) {
      System.out.println("contientVivant(Vivant) vivant1 et vivant2 OK");
    }
    else {
      System.out.println("contientVivant(Vivant) vivant1 et vivant2 KO");
    }

    // Test sortir(nomVivant)
    piece.sortir(vivant1);
    if (!(piece.contientVivant(vivant1.getNom()))) {
      System.out.println("sortir(nomVivant) vivant1 OK");
    }
    else {
      System.out.println("sortir(nomVivant) vivant1 KO");
    }

    // Test sortir(Vivant)
    piece.sortir(vivant2);
    if (!(piece.contientVivant(vivant2))) {
      System.out.println("sortir(Vivant) vivant2 OK");
    }
    else {
      System.out.println("sortir(Vivant) vivant2 KO");
    }

    // Test toString
    piece.entrer(vivant2);
    System.out.println("Test toString");
    System.out.println(piece.toString());

  // Test Exceptions
  Monde planete = new Monde("Terre");
  Piece livingRoom = new Piece("livingRoom", planete);
  Piece cuisine = new Piece("cuisine", planete);
  PiedDeBiche crowbar = new PiedDeBiche("crowbar", planete);
  Vivant jamie = new Vivant("Jamie", planete, pointVie, pointForce, livingRoom, tabPiedDeBiche){};

  // Test retirer(nomObjet) non présent de la Pièce
  // System.out.println("Test retirer(nomObjet) non présent de la Pièce");
  // livingRoom.retirer(crowbar.getNom());

  // Test retirer(Objet) non présent de la Pièce
  // System.out.println("Test retirer(Objet) non présent de la Pièce");
  // livingRoom.retirer(crowbar);

  // Test retirer(nomObjet) non déplaçable
  // System.out.println("Test retirer(nomObjet) non déplaçable");
  // Objet objetQuelconque = new Objet("aglo", planete) {
  //   public boolean estDeplacable() {
  //     return false;
  //   }
  // };
  // livingRoom.deposer(objetQuelconque);
  // livingRoom.retirer(objetQuelconque.getNom());

  // Test retirer(Objet) non déplaçable
  // System.out.println("Test retirer(Objet) non déplaçable");
  // Objet objetQuelconque = new Objet("aglo", planete) {
  //   public boolean estDeplacable() {
  //     return false;
  //   }
  // };
  // livingRoom.deposer(objetQuelconque);
  // livingRoom.retirer(objetQuelconque);

  // Test sortir(nomVivant) non présent dans la Pièce
  // System.out.println("Test retirer(nomVivant) non présent dans la Pièce");
  // cuisine.sortir(jamie.getNom());

  // Test sortir(Vivant) non présent dans la Pièce
  System.out.println("Test retirer(Vivant) non présent dans la Pièce");
  cuisine.sortir(jamie);

  } catch(NomDEntiteDejaUtiliseDansLeMondeException e) {
    e.printStackTrace();
  }
  catch(ObjetAbsentDeLaPieceException e) {
    e.printStackTrace();
  }
  catch(VivantAbsentDeLaPieceException e) {
    e.printStackTrace();
  }
  catch(ObjetNonDeplacableException e) {
    e.printStackTrace();
  }

  }
}
