package fr.insarouen.asi.prog.asiaventure.elements.structure;

/**
* Création de la classe PieceException fille de ElementStructurelException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class PieceException extends ElementStructurelException{

  // Constructeurs

  /**
  * Création de PieceException
  */
  public PieceException() {
  }

  /**
  * Création de PieceException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public PieceException(String msg) {
    super(msg);
  }
}
