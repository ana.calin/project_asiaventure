package fr.insarouen.asi.prog.asiaventure.elements.structure;

/**
* Création de la classe VivantAbsentDeLaPieceException fille de PieceException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class VivantAbsentDeLaPieceException extends PieceException{

  // Constructeurs

  /**
  * Création de VivantAbsentDeLaPieceException
  */
  public VivantAbsentDeLaPieceException() {
  }

  /**
  * Création de VivantAbsentDeLaPieceException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public VivantAbsentDeLaPieceException(String msg) {
    super(msg);
  }
}
