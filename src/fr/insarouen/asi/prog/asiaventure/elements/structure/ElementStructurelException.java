package fr.insarouen.asi.prog.asiaventure.elements.structure;

import fr.insarouen.asi.prog.asiaventure.ASIAventureException;

/**
* Création de la classe ElementStructurelException fille de ASIAventureException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ElementStructurelException extends ASIAventureException{

  // Constructeurs

  /**
  * Création de ElementStructurelException
  */
  public ElementStructurelException() {
  }

  /**
  * Création de ElementStructurelException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ElementStructurelException(String msg) {
    super(msg);
  }

}
