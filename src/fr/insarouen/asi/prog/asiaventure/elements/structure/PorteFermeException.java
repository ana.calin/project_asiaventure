package fr.insarouen.asi.prog.asiaventure.elements.structure;

/**
* Exception lancée lorsque l'on essaye de franchir une porte fermée.
*/

public class PorteFermeException extends ElementStructurelException {
  /**
  * Constructeur de PorteFermeException
  */
  public PorteFermeException() {
  }

  /**
  * Constructeur PorteFermeException
  * @param msg le message de l'exception
  */
  public PorteFermeException(String msg) {
    super(msg);
  }
}
