package fr.insarouen.asi.prog.asiaventure.elements;

/**
* Création de la classe ActivationImpossibleException fille de ActivationException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ActivationImpossibleException extends ActivationException {

  // Constructeurs

  /**
  * Création d'un ActivationImpossibleException
  */
  public ActivationImpossibleException() {
  }

  /**
  * Création d'un ActivationImpossibleException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ActivationImpossibleException(String msg){
    super(msg);
  }
}
