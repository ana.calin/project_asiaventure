package fr.insarouen.asi.prog.asiaventure.elements;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;

/**
* Création de la classe ActivationException fille de ASIAventureException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ActivationException extends ASIAventureException {

  // Constructeurs

  /**
  * Création d'un ActivationException
  */
  public ActivationException() {
	}

  /**
  * Création d'un ActivationException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
	public ActivationException(String msg) {
		super(msg);
	}
}
