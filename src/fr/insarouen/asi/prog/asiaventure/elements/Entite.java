package fr.insarouen.asi.prog.asiaventure.elements;

import fr.insarouen.asi.prog.asiaventure.*;

/**
* Création de la abstraite classe Entite
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public abstract class Entite extends Object implements java.io.Serializable {

  // Attributs

  /**
  * nom de l'Entite
  */
  private String nom;
  /**
  * le Monde
  */
  private Monde monde;

  // Constructeurs

  /**
  * Création d'une Entite
  * @param nom nom de l'Entite
  * @param monde monde où le jeu a lieu
  */
  public Entite (String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    this.nom=nom;
    this.monde=monde;

    try {
      monde.ajouter(this);
    } catch (EntiteDejaDansUnAutreMondeException e) {
      e.printStackTrace();
    }
  }

  // Methodes

  /**
  * Permet d'obtenir le nom d'une Entite
  * @return String
  */
  public String getNom(){
    return this.nom;
  }

  /**
  * Permet d'obtenir le monde d'une Entite
  * @return Monde
  */
  public Monde getMonde(){
    return this.monde;
  }

  /**
  * Permet de transformer le nom et le monde de l'Entite en string
  * @return String
  */
  public String toString() {
    return String.format("Nom %s, Monde %s",this.getNom(),this.getMonde().getNom());
  }

  public boolean equals(Object o) {
    if (o==this) {
      return true;
    }
    if ((o==null) || (this.getClass() != o.getClass())) {
      return false;
    }
      return (this.getNom().equals(((Entite)o).getNom()) && (this.getMonde().getNom().equals(((Entite)o).getMonde().getNom())));
  }

  public int hashCode() {
		return 13*getNom().hashCode() + 17*getMonde().hashCode();
	}
}
