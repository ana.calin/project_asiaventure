package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import java.util.*;
import java.lang.reflect.*;

import fr.insarouen.asi.prog.asiaventure.elements.vivants.JoueurHumain;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.CommandeImpossiblePourLeVivantException;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Porte;
import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;
import  fr.insarouen.asi.prog.asiaventure.elements.structure.VivantAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteInexistanteDansLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteFermeException;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.ObjetNonPossedeParLeVivantException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationException;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.Executable;

public class JoueurHumain extends Vivant implements Executable {

  // Attributs
  
  private String ordreDAction;

  // Constructeurs

  public JoueurHumain(String nom, Monde monde, int pointVie, int pointForce, Piece piece, Objet... objets) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde, pointVie, pointForce, piece, objets);
  }

  // Méthodes

  /**
  * Execute une action
  */
  public void executer() throws CommandeImpossiblePourLeVivantException, Throwable {
    String commandeAExecuter=this.ordreDAction;
    String[] tabCommandeAExecuter=commandeAExecuter.split(" ");
    String[] newArray= new String[tabCommandeAExecuter.length-1];

    // Copie du sous tableau : tabCommandeAExecuter[1:end]
    for (int i=1; i<tabCommandeAExecuter.length; i++) {
      newArray[i-1]=tabCommandeAExecuter[i];
    }

    Class[] newArrayClass= new Class[newArray.length];
    for (int i=0; i<newArray.length; i++){
        newArrayClass[i] = newArray[i].getClass();
    }

    try {
      Method method=(JoueurHumain.class).getDeclaredMethod("commande"+tabCommandeAExecuter[0], newArrayClass);
      method.invoke((Object)this, (Object[])newArray);
    } catch (NoSuchMethodException e) { // if fromIndex < 0 or toIndex > a.length
      throw new CommandeImpossiblePourLeVivantException("La commande "+ordreDAction+" n'existe pas");
    }
    catch (InvocationTargetException e) {
      throw e.getCause();
    }
    catch (IllegalArgumentException e) {
      throw new CommandeImpossiblePourLeVivantException("Vous n'avez pas entré le bon nombre de paramamètre pour cette action du JoueurHumain");
    }
  }

  /**
  * Permet de définir l'ordre d'action
  * @param ordre l'ordre d'action
  */
  public void setOrdre(String ordre) {
    this.ordreDAction=ordre;
  }

  /**
  * Permet de prendre un objet
  * @param nomObjet nom de l'objet
  */
  private void commandePrendre(String nomObjet) throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
    super.prendre(nomObjet);
  }

  /**
  * Permet de poser un Objet
  * @param nomObjet nom de la Porte
  */
  private void commandePoser(String nomObjet) throws ObjetNonPossedeParLeVivantException {
    super.deposer(nomObjet);
  }

  /**
  * Permet de franchier une Porte
  * @param nomPorte nom de la Porte
  */
  private void commandeFranchir(String nomPorte) throws PorteFermeException, PorteInexistanteDansLaPieceException, VivantAbsentDeLaPieceException {
    super.franchir(nomPorte);
  }

  /**
  * Permet d'ourvir une porte
  * @param nomPorte nom de la Porte
  */
  private void commandeOuvrirPorte(String nomPorte) throws ActivationException, PorteInexistanteDansLaPieceException {
    super.getPiece().getPorte(nomPorte).activer();
  }

  /**
  * Permet d'ourvir une porte avec un Objet
  * @param nomPorte nom de la Porte
  * @param nomObjet nom de la Objet
  */
  private void commandeOuvrirPorte(String nomPorte, String nomObjet) throws ActivationException, PorteInexistanteDansLaPieceException, ObjetNonPossedeParLeVivantException {
    super.getPiece().getPorte(nomPorte).activerAvec(super.getObjet(nomObjet));
  }

  /**
  * Permet de décrire le JoueurHumain
  * @return String
  */
  public String toString() {
    return super.toString();
  }
}
