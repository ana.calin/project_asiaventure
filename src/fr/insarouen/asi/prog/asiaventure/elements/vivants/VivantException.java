package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import fr.insarouen.asi.prog.asiaventure.ASIAventureException;

/**
* Création de la classe VivantException fille de ASIAventureException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class VivantException extends ASIAventureException {

  // Constructeurs

  /**
  * Création de VivantException
  */
  public VivantException() {
  }

  /**
  * Création de VivantException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public VivantException(String msg) {
    super(msg);
  }
}
