package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import  fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.ObjetNonPossedeParLeVivantException;

/**
* Création de la classe ClassMainTestVivant
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ClassMainTestVivant {

  /**
  * Main pour ClassMainTestVivant
  * @param args les parametres qui peuvent etre entres via l'entree standard
  */
  public static void main(String[] args) {

  try {
    // Tests classiques
    Monde m1 = new Monde("toto");
    int pointVie = 10;
    int pointForce = 10;
    Piece piece = new Piece("salon", m1);
    PiedDeBiche piedDeBiche1 = new PiedDeBiche("crowbar1", m1);
    PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", m1);
    PiedDeBiche piedDeBiche3 = new PiedDeBiche("crowbar3", m1);
    PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[0];

    piece.deposer(piedDeBiche1);
    piece.deposer(piedDeBiche2);
    piece.deposer(piedDeBiche3);

    Vivant vivant = new Vivant("homme", m1, pointVie, pointForce, piece, tabPiedDeBiche){};

    // Test prendre(Objet)
    vivant.prendre(piedDeBiche1);
    if (piece.contientObjet(piedDeBiche1)==false) {
      System.out.println("prendre(Objet) piedDeBiche1 OK");
    }
    else {
      System.out.println("prendre(Objet) piedDeBiche1 KO");
    }

    // Test prendre(Objet)
    vivant.prendre(piedDeBiche3);
    if (piece.contientObjet(piedDeBiche3)==false) {
      System.out.println("prendre(Objet) piedDeBiche3 OK");
    }
    else {
      System.out.println("prendre(Objet) piedDeBiche3 KO");
    }

    // Test getObjet
    if (vivant.getObjet("crowbar1").equals(piedDeBiche1)) {
      System.out.println("getObjet OK");
    }
    else {
      System.out.println("getObjet KO");
    }

    if (vivant.getObjet("crowbar2")==null) {
      System.out.println("getObjet OK");
    }
    else {
      System.out.println("getObjet KO");
    }

    // Test possede
    if (vivant.possede(piedDeBiche1)) {
      System.out.println("possede piedDeBiche1 OK");
    }
    else {
      System.out.println("possede piedDeBiche1 KO");
    }

    // Test prendre(nomObjet)
    vivant.prendre(piedDeBiche2.getNom());
    if (piece.contientObjet(piedDeBiche2.getNom())==false) {
      System.out.println("prendre(nomObjet) OK");
    }
    else {
      System.out.println("prendre(nomObjet) KO");
    }

    // Test possede
    if (vivant.possede(piedDeBiche1)) {
      System.out.println("possede piedDeBiche2 OK");
    }
    else {
      System.out.println("possede piedDeBiche2 KO");
    }

    // Test deposer(Objet)
    vivant.deposer(piedDeBiche2);
    if ((piece.contientObjet(piedDeBiche2)==true) && (vivant.possede(piedDeBiche2)==false)) {
      System.out.println("deposer(Objet) piedDeBiche2 OK");
    }
    else {
      System.out.println("deposer(Objet) piedDeBiche2 KO");
    }

    // Test deposer(nomObjet)
    vivant.deposer(piedDeBiche3.getNom());
    if ((piece.contientObjet(piedDeBiche3.getNom())==true) && (vivant.possede(piedDeBiche3)==false)) {
      System.out.println("deposer(nomObjet) piedDeBiche3 OK");
    }
    else {
      System.out.println("deposer(nomObjet) piedDeBiche3 KO");
    }

    // Test getPiece
    if (vivant.getPiece().equals(piece)) {
      System.out.println("getPiece OK");
    }
    else {
      System.out.println("getPiece KO");
    }

    // Test getPointVie
    if (vivant.getPointVie()==10) {
      System.out.println("getPointVie OK");
    }
    else {
      System.out.println("getPointVie KO");
    }

    // Test getPointForce
    if (vivant.getPointForce()==10) {
      System.out.println("getPointForce OK");
    }
    else {
      System.out.println("getPointForce KO");
    }

    // Test estMort
    if (vivant.estMort()==false) {
      System.out.println("estMort OK");
    }
    else {
      System.out.println("estMort KO");
    }

    // Test toString
    vivant.prendre(piedDeBiche3);
    System.out.println("Test toString");
    System.out.println(vivant.toString());

  // Test Exceptions
    Monde planete = new Monde("Terre");
    Piece livingRoom = new Piece("livingRoom", planete);
    Vivant jamie = new Vivant("Jamie", planete, pointVie, pointForce, livingRoom, tabPiedDeBiche){};
    PiedDeBiche piedDeBiche = new PiedDeBiche("crowbar", planete);

    // Test prendre(Objet) non présent dans la pièce
    // System.out.println("Test prendre(Objet) non présent dans la pièce");
    // jamie.prendre(piedDeBiche);

    // Test prendre(nomObjet) non présent dans la pièce
    // System.out.println("Test prendre(nomObjet) non présent dans la pièce");
    //jamie.prendre(piedDeBiche.getNom());

    // Test prendre(nomObjet) non déplaçable
    //System.out.println("Test prendre(nomObjet) non déplaçable");
    // Objet objetQuelconque = new Objet("aglo", planete) {
    //   public boolean estDeplacable() {
    //     return false;
    //   }
    // };
    // livingRoom.deposer(objetQuelconque);
    // jamie.prendre(objetQuelconque.getNom());

    // Test prendre(Objet) non déplaçable
    // System.out.println("Test prendre(Objet) non déplaçable");
    // Objet objetQuelconque = new Objet("aglo", planete) {
    //   public boolean estDeplacable() {
    //     return false;
    //   }
    // };
    // livingRoom.deposer(objetQuelconque);
    // jamie.prendre(objetQuelconque);

    // Test deposer(nomObjet) non possédé par le Vivant
    // System.out.println("Test deposer(nomObjet) non possédé par le Vivant");
    // jamie.deposer(piedDeBiche.getNom());

    // Test deposer(Objet) non possédé par le Vivant
    System.out.println("Test deposer(Objet) non possédé par le Vivant");
    jamie.deposer(piedDeBiche);

  } catch(NomDEntiteDejaUtiliseDansLeMondeException e) {
    e.printStackTrace();
  }
  catch(ObjetAbsentDeLaPieceException e) {
    e.printStackTrace();
  }
  catch(ObjetNonDeplacableException e) {
    e.printStackTrace();
  }
  catch(ObjetNonPossedeParLeVivantException e) {
    e.printStackTrace();
  }

  }
}
