package fr.insarouen.asi.prog.asiaventure.elements.vivants;

/**
* Création de la classe ObjetNonPossedeParLeVivantException fille de VivantException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ObjetNonPossedeParLeVivantException extends VivantException {

  /**
  * Création de ObjetNonPossedeParLeVivantException
  */
  public ObjetNonPossedeParLeVivantException() {
  }

  /**
  * Création de ObjetNonPossedeParLeVivantException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ObjetNonPossedeParLeVivantException(String msg) {
    super(msg);
  }
}
