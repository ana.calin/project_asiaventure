package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.Etat;
import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.elements.Executable;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Porte;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.ObjetNonPossedeParLeVivantException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteFermeException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.VivantAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteInexistanteDansLaPieceException;
import java.util.*;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationException;
import fr.insarouen.asi.prog.asiaventure.elements.Activable;
import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Serrure;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;

/**
* Création de la classe Vivant fille de Entite et implémente Executable
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public abstract class Vivant extends Entite { //implements Executable

  // Attributs

  /**
  * Points de vie d'un Vivant
  */
  private int pointVie;
  /**
  * Points de force d'un Vivant
  */
  private int pointForce;
  /**
  * Piece où se situe le Vivant
  */
  private Piece piece;
  /**
  * Collection d'Entite dans ce Monde
  */
  private HashMap<String, Objet> mapObjet;

  // Constructeurs

  /**
  * Création d'un Vivant
  * @param nom Nom du Vivant
  * @param monde le Monde
  * @param pointVie Points de vie d'un Vivant
  * @param pointForce Points de force d'un Vivant
  * @param piece Piece où se situe le Vivant
  * @param objets Tableau d'Objets (inventaire) qui contient les Objets que le vivant possede
  */
  public Vivant(String nom, Monde monde, int pointVie, int pointForce, Piece piece, Objet... objets) throws NomDEntiteDejaUtiliseDansLeMondeException { // ... comme [] en gros
    super(nom, monde);
    this.piece=piece;
    this.pointVie=pointVie;
    this.pointForce=pointForce;
    this.mapObjet = new HashMap<String, Objet>();

    for (int i=0; i<objets.length; i++) {
      this.mapObjet.put(objets[i].getNom(), objets[i]);
    }

    this.piece.entrer(this);
  }

  public void setPointsDeVie(int pointsDeVie){
    this.pointVie=pointsDeVie;
  }

  // Méthodes

  /**
	* Permet de prendre un Objet de la Piece, grammeSimulation Entites Entl'enlever de la Piece et de l'ajouter dans l'inventaire.
	* @param obj Objet qu'on prend
	*/
  public void prendre(Objet obj) throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {

    if (!(this.getPiece().contientObjet(obj))) {
          throw new ObjetAbsentDeLaPieceException("L'objet demandé n'est pas présent dans la pièce");
    }

    if (!(obj.estDeplacable())) {
          throw new ObjetNonDeplacableException("L'objet demandé n'est pas déplaçable");
    }

    this.mapObjet.put(obj.getNom(), obj);

    this.piece.retirer(obj);
  }

  /**
	* Permet de prendre un Objet de la Piece, l'enlever de la Piece et de l'ajouter dans l'inventaire.
	* @param nomObj nom de l'Objet qu'on prend
	*/
  public void prendre(String nomObj) throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
    Objet lObjet;

    if (!(this.getPiece().contientObjet(nomObj))) {
          throw new ObjetAbsentDeLaPieceException("L'objet demandé n'est pas présent dans la pièce");
    }

    if (!(this.piece.getObjet(nomObj).estDeplacable())) {
          throw new ObjetNonDeplacableException("L'objet demandé n'est pas déplaçable");
    }

    lObjet=this.piece.getObjet(nomObj); // getObjet dans la pièce n'est pas dans la javadoc mais je ne savais comment faire autrement

    prendre(lObjet);
  }

  /**
  * Permet de déposer un objet dans la Piece et donc de retirer l'Objet de l'inventaire du Vivant
  * @param nomObj nom de l'objet
  */
   public void deposer(String nomObj) throws ObjetNonPossedeParLeVivantException {


     if (!(possede(getObjet(nomObj)))) {
       throw new ObjetNonPossedeParLeVivantException("Vous ne pouvez pas déposer l'Objet "+nomObj+" car il n'est pas possédé par le Vivant");
     }

     this.piece.deposer(mapObjet.get(nomObj));
     this.mapObjet.remove(nomObj);

  }

  /**
  * Permet de déposer un objet dans la Piece et donc de retirer l'objet dans l'inventaire du Vivant
  * @param obj un objet
  */
  public void deposer(Objet obj) throws ObjetNonPossedeParLeVivantException {
        deposer(obj.getNom());
  }

  /**
  * Permet d'obtenir la Piece dans laquelle se trouve le Vivant
  * @return Piece la Piece
  */
  public Piece getPiece() {
    return this.piece;
  }

  /**
  * Permet d'obtenir un Objet à partir de son nom dans l'inventaire du Vivant
  * @param nomObjet le nom de l'Objet
  * @return Objet l'Objet voulu (null si le Vivant ne possede pas l'Objet)
  */
  public Objet getObjet(String nomObjet) {
    return this.mapObjet.get(nomObjet);
  }

  /**
	* Permet de savoir si un objet est présent dans l'inventaire
	* @param obj l'Objet à tester
	* @return boolean
	*/
	public boolean possede(Objet obj) {
		return getObjet(obj.getNom()) != null;
	}

  /**
  * Permet d'obtenir les points de vie du Vivant
  * @return int les points de vie
  */
  public int getPointVie() {
    return this.pointVie;
  }

  /**
  * Permet d'obtenir les points de force du Vivant
  * @return int les points de force
  */
  public int getPointForce() {
    return this.pointForce;
  }

  /**
  * Permet de savoir si un Vivant est mort
  * @return boolean
  */
  public boolean estMort() {
    return (this.pointVie==0);
  }

  /**
  * Permet de franchir une porte
  * @param porte
  */
  public void franchir(Porte porte) throws PorteFermeException, PorteInexistanteDansLaPieceException{
      franchir(porte.getNom());
  }

  /**
  * Permet de franchir une porte a partir de son nom
  * @param nomPorte
  */
  public void franchir(String nomPorte) throws PorteFermeException, PorteInexistanteDansLaPieceException {
   Porte porte;

   porte=this.getPiece().getPorte(nomPorte);

   if (this.getPiece().aLaPorte(nomPorte) == false) {
     throw new PorteInexistanteDansLaPieceException("La porte "+nomPorte+" n'existe pas dans la Piece "); //+this.piece.toString()
   }
   else if (porte.getEtat() == Etat.FERME) {
     throw new PorteFermeException("La porte "+nomPorte+" est "+porte.getEtat());
   }
   else if ((porte.getEtat() == Etat.OUVERT) || (porte.getEtat() == Etat.CASSE)) {
    porte.getPieceAutreCote(this.getPiece()).entrer(this);

     try{
       this.getPiece().sortir(this);
       this.piece = porte.getPieceAutreCote(getPiece());
     } catch (VivantAbsentDeLaPieceException e) {
       e.printStackTrace();
       // throw new RuntimeException("Le Vivant est absent de la Piece");
     }
   }
  }

  /**
  * Permet de décrire le Vivant
  * @return String
  */
  public String toString() {
    StringBuilder s=new StringBuilder();
    s.append("Le vivant : ").append(this.getNom()).append("\n");
    s.append("Point de vie : ").append(this.getPointVie()).append("\n");
    s.append("Point de Force : ").append(this.getPointForce()).append("\n");
    s.append("Nom de la Pièce : ").append(this.getPiece().getNom()).append("\n");
    s.append("Objets dans la Pièce : ").append(this.getPiece().getObjets()).append("\n");

    Collection<Porte> lesPortesDeLaPiece = this.getPiece().getPortes();

    s.append("Porte dans la Pièce : ");
    for (Porte p : lesPortesDeLaPiece) {
      if (p.getSerrure()!=null) {
        s.append(p.getNom()).append(": ").append(p.getEtat()).append(": ").append(p.getSerrure().getNomClef()).append(", ");
      }
      else {
        s.append(p.getNom()).append(": ").append(p.getEtat()).append(", ");
      }

    }
    s.append("\n");

    s.append("Le vivant possède ces objets : \n");
    for (Objet o : this.mapObjet.values()) {
			s.append(o.toString()).append("\n");
		}

    return s.toString();
  }

  /**
  * Permet d'obtenir le nom des Objets et tous les Objets présent dans l'inventaire du Vivant
  * @return Map<String, Objet> le nom des Objets et les Objets
  */
  public Map<String, Objet> getObjets() {
    return Collections.unmodifiableMap(this.mapObjet);
  }

  /**
  * Permet au Vivant d'activer un Activable
  * @param activable un Activable
  */
  public void activerActivable(Activable activable) throws ActivationException {
    activable.activer();
  }

  /**
  * Permet au Vivant d'activer un Activable aver un Objet
  * @param activable un Activable
  * @param objet un Objet
  */
  public void activerActivableAvecObjet(Activable activable, Objet objet) throws ActivationException {
    activable.activerAvec(objet);
  }

  // /**
  // * Permet d'Excuter (pas implémenté)
  // */
  // void executer() throws Throwable {
  //   // New
  //   throw new UnsupportedOperationException("Cette méthode n'est pas implémentée dans Vivant");
  // }
}
