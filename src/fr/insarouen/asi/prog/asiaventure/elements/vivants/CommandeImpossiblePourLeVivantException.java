package fr.insarouen.asi.prog.asiaventure.elements.vivants;

/**
* Création de la classe CommandeImpossiblePourLeVivantException fille de VivantException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class CommandeImpossiblePourLeVivantException extends VivantException {

  // Constructeurs

  /**
  * Création de CommandeImpossiblePourLeVivantException
  */
  public CommandeImpossiblePourLeVivantException() {
  }

  /**
  * Création de CommandeImpossiblePourLeVivantException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public CommandeImpossiblePourLeVivantException(String msg) {
    super(msg);
  }
}
