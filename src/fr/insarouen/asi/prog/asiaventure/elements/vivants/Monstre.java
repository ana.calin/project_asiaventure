package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import java.util.*;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import java.lang.Math;
import java.util.Random;
import java.util.ArrayList;

public class Monstre extends Vivant {

  // Attributs

  /**
  * Points de vie d'un Vivant
  */
  private int pointVieM;
  /**
  * Points de force d'un Vivant
  */
  private int pointForceM;
  /**
  * Piece où se situe le Vivant
  */
  private Piece pieceM;
  /**
  * Tableau d'Objets qui contient les Objets que le vivant possede
  */
  // private Objet[] tabObjets;

  // Constructeurs

  /**
  * Création d'un Monstre
  * @param nomM Nom du Monstre
  * @param mondeM Ne Monde
  * @param pointVieM Points de vie d'un Monstre
  * @param pointForceM Points de force d'un Monstre
  * @param pieceM Piece où se situe le Monstre
  * @param tabObjetsM Tableau d'Objets (inventaire) qui contient les Objets que le Monstre possede
  */
  public Monstre(String nomM, Monde mondeM, int pointVieM, int pointForceM, Piece pieceM, Objet... tabObjetsM) throws NomDEntiteDejaUtiliseDansLeMondeException {
		  super(nomM, mondeM, pointVieM, pointForceM, pieceM, tabObjetsM);
	}

  // Méthodes

  /**
  * Permet de deposer le objets que le Monstre possede et d'attraper les objets de la piece
  */
  public void attraperEtDeposerObjets() throws ObjetNonPossedeParLeVivantException, ObjetNonDeplacableException, ObjetAbsentDeLaPieceException {
    Collection<Objet> objetsADeposer = new ArrayList<Objet>(this.getObjets().values());
    Collection<Objet> objetsAAttraper= new ArrayList<Objet>(this.getPiece().getObjets());

    Iterator it = objetsAAttraper.iterator();
    Iterator it2 = objetsADeposer.iterator();

    while(it.hasNext()) {
      Objet objetAAttraper = (Objet)it.next();
      if(objetAAttraper.estDeplacable())
        this.prendre(objetAAttraper);
    }

    while(it2.hasNext()) {
      Objet objetADeposer = (Objet)it2.next();
      this.deposer(objetADeposer);
    }
  }

  /**
  * Permet au Monstre d'exécuter une action
  */
  public void executer() throws ActivationImpossibleException,PorteFermeException, PorteInexistanteDansLaPieceException,ObjetNonPossedeParLeVivantException, ObjetNonDeplacableException, ObjetAbsentDeLaPieceException {

      Porte porteChoisie;
      if(!this.estMort()){
        List<Porte> portesDeLaPiece = new ArrayList<Porte>(this.getPiece().getPortes());
        Collections.shuffle(portesDeLaPiece);
        porteChoisie = portesDeLaPiece.get(0);
        if (porteChoisie.getEtat()!=Etat.VERROUILLE){
        if (porteChoisie.getEtat()==Etat.FERME || porteChoisie.getEtat()==Etat.CASSE || porteChoisie.getEtat()==Etat.OUVERT) {
          if (porteChoisie.getEtat()==Etat.FERME){
            porteChoisie.activer();}
          this.franchir(porteChoisie);
          this.attraperEtDeposerObjets();
        }
      }
        this.setPointsDeVie(this.getPointVie()-1);
      }
  }

  /**
   * Permet de décrire le Monstre
  * @return String
  */
  public String toString() {
  		StringBuilder sb = new StringBuilder();
  		sb.append("Monstre [" + super.toString() + "]");
  		return sb.toString();
  	}
}
