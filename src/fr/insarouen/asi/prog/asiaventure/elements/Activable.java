package fr.insarouen.asi.prog.asiaventure.elements;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;

/**
* Création de la'interface Activable
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public interface Activable {

	// Méthodes

	/**
	* Permet de savoir si un objet permet l'activation de la porte courante.
	* @param obj Objet a tester
	* @return boolean
	*/
	boolean activableAvec(Objet obj);

	/**
	* Active la porte
	*/
	void activer() throws ActivationException;

	/**
	* Permet d'activer une porte à l'aide d'un objet.
	* @param obj Objet permettant d'activer la porte
	*/
	void activerAvec(Objet obj) throws ActivationException;

	/**
	* Permet d'obtenir l'Etat la Porte
	* @return Etat l'etat de la porte
	*/
	Etat getEtat();
}
