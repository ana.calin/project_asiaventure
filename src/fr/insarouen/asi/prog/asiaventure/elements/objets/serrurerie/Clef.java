package fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

public final class Clef extends Objet {

  // Constructeur

  /**
  * Création d'une Clef
  * @param nom nom
  * @param monde monde
  */
  protected Clef(String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde);
  }

  // Méthodes

  /**
  * Permet de savoir si une Clef est deplacable
  * @return boolean
  */
  public boolean estDeplacable() {
    return true;
  }

  /**
  * Permet de décrire la Clef
  * @return String
  */
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(super.toString()).append("\n");

    return sb.toString();
  }
}
