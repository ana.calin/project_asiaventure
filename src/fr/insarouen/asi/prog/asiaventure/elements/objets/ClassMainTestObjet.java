package fr.insarouen.asi.prog.asiaventure.elements.objets;

import  fr.insarouen.asi.prog.asiaventure.elements.Entite;
import  fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

/**
* Création de la classe ClassMainTestObjet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ClassMainTestObjet {

  /**
  * Main pour ClassMainTestObjet
  * @param args les parametres qui peuvent etre entres via l'entree standard
  */
  public static void main(String[] args){
    try {
    // Test constructeur Objet
    Monde m1 = new Monde("toto");
    Monde m2 = new Monde("toto");

		Objet o1 = new Objet("o1",m1){public boolean estDeplacable() {return false;}};
		Objet o2 = new Objet("o1",m2){public boolean estDeplacable() {return false;}};

    if (o1.equals(o2)) {
      System.out.println("Constructeur Objet OK");
    }
    else {
      System.out.println("Constructeur Objet OK car Entite est abstract");
    }

    // Test estDeplacable
    if (o1.estDeplacable()==false) {
      System.out.println("estDeplacable OK");
    }
    else {
      System.out.println("estDeplacable KO");
    }

    // Test toString
    System.out.println(o1.toString());

    // Test création de deux Objets avec le même nom dans le même MondeException
    Monde planete = new Monde("Terre");

		Objet objet1 = new Objet("objet", planete){public boolean estDeplacable() {return false;}};
		Objet objet2 = new Objet("objet", planete){public boolean estDeplacable() {return false;}};

    } catch (NomDEntiteDejaUtiliseDansLeMondeException e) {
      e.printStackTrace();
    }

  }
}
