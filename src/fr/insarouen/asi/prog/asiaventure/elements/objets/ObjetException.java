package fr.insarouen.asi.prog.asiaventure.elements.objets;

import fr.insarouen.asi.prog.asiaventure.ASIAventureException;

/**
* Création de la classe ObjetException fille de ASIAventureException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ObjetException extends ASIAventureException {

  // Constructeurs

  /**
  * Création de ObjetException
  */
  public ObjetException() {
  }

  /**
  * Création de ObjetException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ObjetException(String msg) {
    super(msg);
  }
}
