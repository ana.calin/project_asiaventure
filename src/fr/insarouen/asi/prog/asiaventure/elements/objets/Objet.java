package fr.insarouen.asi.prog.asiaventure.elements.objets;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

/**
* Création de la classe abstraite Objet fille de Entite
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public abstract class Objet extends Entite {

  // Constructeurs

  /**
  * Création d'un Objet
  * @param nom nom
  * @param monde monde
  */
  public Objet (String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde);
  }

  // Methodes

  /**
  * Permet de savoir si un objet est deplacable (méthode abstraite)
  * @return boolean
  */
  public abstract boolean estDeplacable();

  /**
  * Permet de transformer le nom et le monde de l'Objet en string
  * @return String
  */
  public String toString() {
		return super.toString()+", estDeplacable : "+this.estDeplacable();
	}
}
