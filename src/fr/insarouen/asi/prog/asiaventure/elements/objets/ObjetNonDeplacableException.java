package fr.insarouen.asi.prog.asiaventure.elements.objets;

/**
* Création de la classe ObjetNonDeplacableException fille de ObjetException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ObjetNonDeplacableException extends ObjetException {

  // Constructeurs

  /**
  * Création de ObjetNonDeplacableException
  */
  public ObjetNonDeplacableException() {
  }

  /**
  * Création de ObjetNonDeplacableException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ObjetNonDeplacableException(String msg) {
    super(msg);
  }
}
