package fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.Activable;
import fr.insarouen.asi.prog.asiaventure.elements.Etat;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleAvecObjetException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleException;

public class Serrure extends Objet implements Activable {

  // Attributs

  /**
  * Etat de la Serrure
  */
  private Etat etat = Etat.DEVERROUILLE;
  /**
  * Nom de la Clef unique correspondant à la Serrure
  */
  private String nomClef;
  /**
  * Numero de la Clef
  */
  private static int numeroClef=1;
  /**
  * Numero de la Serrure
  */
  private static int numeroSerrure=1;
  /**
  * Booléen permettant de savoir si la Clef a déjà été crée pour cette Serrure
  */
  private boolean clefDejaCree=false;

  // Constructeurs

  /**
  * Création d'une Serrure
  * @param monde monde
  */
  public Serrure(Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(genererNomSerrureUnique(monde), monde);
  }

  private static String genererNomSerrureUnique(Monde monde) {
    String prefixe = "serrure";

    while (monde.getEntite(prefixe+numeroSerrure) != null) {
      numeroSerrure++;
    }

    return prefixe+numeroSerrure;
  }

  /**
  * Création d'une Serrure
  * @param nom nom de la Serrure
  * @param monde monde
  */
  public Serrure(String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde);
  }

  // Méthodes

  /**
  * Permet de creer une Clef unique pour une Serrure
  * @return Clef
  */
  public final Clef creerClef() {

    if (this.clefDejaCree == false) {
      this.clefDejaCree = true;
      this.nomClef=genererNomClefUnique(this.getMonde());
      try {
        return new Clef(this.nomClef, this.getMonde());
      } catch (NomDEntiteDejaUtiliseDansLeMondeException e) {
        e.printStackTrace();
      }
    }
    return null;
  }

  private static String genererNomClefUnique(Monde monde) {
    String prefixe = "clef";

    while (monde.getEntite(prefixe+numeroClef) != null) {
      numeroClef++;
    }
    return prefixe+numeroClef;
  }

  /**
  * Permet d'activer une Serrure
  * @param obj un Objet (une Clef en fait)
  */
  public void activerAvec(Objet obj) throws ActivationImpossibleAvecObjetException { // l'objet correspond à la clef

    // On vérifie si l'Objet est bien une Clef
    if (!(obj instanceof Clef)) {
      throw new ActivationImpossibleAvecObjetException("Vous ne pouvez pas activer la Serrure car l'Objet utilisé n'est pas une Clef");
    }

    if (!(activableAvec(obj))) {
      throw new ActivationImpossibleAvecObjetException("Vous ne pouvez pas activer la Serrure car la Clef ne correspond pas à la Serrure");
    }

    if (getEtat() == Etat.DEVERROUILLE) {
      this.etat = Etat.VERROUILLE;
    }
    else {
      if (getEtat() == Etat.VERROUILLE) {
        this.etat = Etat.DEVERROUILLE;
      }
    }
  }

  /**
  * Permet de savoir si une Clef est deplacable
  * @return boolean
  */
  public boolean estDeplacable() {
    return false;
  }

  /**
  * Permet de savoir la Serrure est activable avec un Objet
  * @return boolean
  */
  public boolean activableAvec(Objet obj) { // l'objet correspond à la clef
    return ((obj.getNom().equals(this.nomClef)) && (obj instanceof Clef));
  }

  public void activer() throws ActivationImpossibleException {
    throw new ActivationImpossibleException("On ne peut pas acitiver une serrure sans clef");
  }

  public Etat getEtat() {
    return this.etat;
  }

  /**
  * Permet de décrire la Serrure
  * @return String
  */
  public String toString() {
    StringBuilder sb = new StringBuilder();

    sb.append(super.toString()).append("\n");;

    sb.append("Etat Serrure : ").append(this.etat).append("\n");
    sb.append("Nom clef associée à Serrure : ").append(this.nomClef).append("\n");
    sb.append("clefDejaCree : ").append(this.clefDejaCree).append("\n");

    return sb.toString();
  }

  /**
  * Permet de récupérer le nom de la clef associé à la Serrure
  * @return String
  */
  public String getNomClef() {
    return this.nomClef;
  }
}
