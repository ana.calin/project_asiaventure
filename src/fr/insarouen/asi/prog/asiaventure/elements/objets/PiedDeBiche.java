package fr.insarouen.asi.prog.asiaventure.elements.objets;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.Monde;

/**
* Création de la classe PiedDeBiche fille de Objet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class PiedDeBiche extends Objet {

	// Constructeur

  /**
  * Création d'un PiedDeBiche
  * @param nom nom
  * @param monde monde
  */
	public PiedDeBiche (String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
		super(nom, monde);
	}

	// Methodes

  /**
  * Permet de savoir si un pied de biche est deplacable
  * @return boolean
  */
	public boolean estDeplacable() {
		return true;
	}

	/**
	* Permet de transformer le nom et le monde de PiedDeBiche en string
	* @return String
	*/
	public String toString() {
		return super.toString();
	}
}
