package fr.insarouen.asi.prog.asiaventure.elements.objets;

import  fr.insarouen.asi.prog.asiaventure.elements.Entite;
import  fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

/**
* Création de la classe ClassMainTestPiedDeBiche
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ClassMainTestPiedDeBiche {

  /**
  * Main pour ClassMainTestPiedDeBiche
  * @param args les parametres qui peuvent etre entres via l'entree standard
  */
  public static void main(String[] args){

    try {
    // Test constructeur Objet
    Monde m1 = new Monde("toto");
    Monde m2 = new Monde("toto");

		PiedDeBiche p1 = new PiedDeBiche("p1",m1);
		PiedDeBiche p2 = new PiedDeBiche("p1",m2);

    if (p1.equals(p2)) {
      System.out.println("Constructeur PiedDeBiche OK");
    }
    else {
      System.out.println("Constructeur PiedDeBiche KO");
    }

    // Test estDeplacable
    if (p1.estDeplacable()==true) {
      System.out.println("estDeplacable OK");
    }
    else {
      System.out.println("estDeplacable KO");
    }

    // Test toString
    System.out.println(p1.toString());

    // Test création deux PiedDeBiche de même nom dans le même Monde
    Monde planete = new Monde("toto");

    PiedDeBiche p3 = new PiedDeBiche("p3", planete);
    PiedDeBiche p4 = new PiedDeBiche("p3", planete);

  } catch (NomDEntiteDejaUtiliseDansLeMondeException e) {
    e.printStackTrace();
  }

  }
}
