package fr.insarouen.asi.prog.asiaventure.elements.objets;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;
/**
* Création de la classe Coffre fille de Objet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/

public class Coffre extends Objet implements Activable {

  private Etat etat;

  // Constructeur

  /**
  * Création d'un Coffre
  * @param nom nom du Coffre
  * @param monde monde le Monde
  */
  public Coffre(String nom, Monde monde) throws NomDEntiteDejaUtiliseDansLeMondeException {
    super(nom, monde);
    etat = Etat.FERME;
  }

  // Méthodes

  /**
  * Permet de savoir si un coffre est deplacable
  * @return boolean
  */
  @Override
  public boolean estDeplacable(){
     return false;
  }

  /**
	* Permet de savoir si un objet permet l'activation de la porte courante.
	* @param obj Objet a tester
	* @return boolean
	*/
  public boolean activableAvec(Objet obj){
   return false;
  }

  /**
	* Active le coffre
	*/
  public void activer() throws ActivationException{
    if (etat == Etat.CASSE) {
			throw new ActivationException();
		}

    if (etat == Etat.FERME){
      etat=Etat.OUVERT;
    }
    else if (etat == Etat.OUVERT){
      etat = Etat.FERME;
    }
  }

  /**
	* Permet d'activer un Coffre à l'aide d'un objet.
	* @param obj Objet permettant d'activer la porte
	*/
  public void activerAvec(Objet obj) throws ActivationException{
    throw new ActivationImpossibleAvecObjetException();
  }

  /**
	* Permet d'obtenir l'Etat du Coffre
	* @return Etat l'etat du Coffre
	*/
  public Etat getEtat(){
    return this.etat;
  }

  /**
  * Permet de décrire le Coffre
  * @return String
  */
  @Override
  public String toString(){
    StringBuilder sb = new StringBuilder();

		sb.append("Coffre [" + super.toString() + "]").append("\n");
    sb.append("Etat du Coffre : ").append(this.etat).append("\n");

		return sb.toString();
  }
}
