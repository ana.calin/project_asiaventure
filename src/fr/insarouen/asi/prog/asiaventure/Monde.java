// Cle : String (nom de l'Entite)
// Valeur : Entite

package fr.insarouen.asi.prog.asiaventure;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;

import fr.insarouen.asi.prog.asiaventure.elements.Executable;
import fr.insarouen.asi.prog.asiaventure.EntiteDejaDansUnAutreMondeException;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import java.util.*;

/**
* Création de la classe Monde
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class Monde extends Object implements java.io.Serializable {

  // Attributs

  /**
  * Nom du Monde
  */
  private String nomDuMonde;

  /**
  * Collection d'Entite dans ce Monde
  */
  private HashMap<String, Entite> mapEntite;

  // Constructeurs

  /**
  * Création d'un Monde
  * @param nomMonde monde où le jeu a lieu
  */
  public Monde(String nomMonde){
    this.nomDuMonde=nomMonde;
    this.mapEntite= new HashMap<String, Entite>();
  }

  // Methodes

  /**
  * Permet d'obtenir le nom du monde
  * @return String
  */
  public String getNom() {
    return this.nomDuMonde;
  }

  public Entite getEntite(String nomEntite) {
     return this.mapEntite.get(nomEntite);
  }

  public void ajouter(Entite entite) throws NomDEntiteDejaUtiliseDansLeMondeException,EntiteDejaDansUnAutreMondeException {

      if (this.getEntite(entite.getNom())!=null)
        throw new NomDEntiteDejaUtiliseDansLeMondeException("Vous ne pouvez pas ajouter l'Entite "+entite.getNom()+" car elle existe déjà dans ce Monde");

      if (entite.getMonde()!=this)
        throw new EntiteDejaDansUnAutreMondeException("Vous ne pouvez pas ajouter l'Entite "+entite.getNom()+" car elle existe déjà dans un autre Monde");

    this.mapEntite.put(entite.getNom(), entite);
  }

  /**
  * Permet d'obtenir les elements executables du monde
  * @return Collection
  */
  public Collection<Executable> getExecutables() {
		Collection<Executable> c = new ArrayList<Executable>();

		for (String s : mapEntite.keySet()) {
			if (mapEntite.get(s) instanceof Executable) {
				c.add((Executable)mapEntite.get(s));
			}
		}
		return c;
	}

  /**
  * Permet d'obtenir une chaine de caractere décrivant un monde (nom du monde et les noms de ses entites)
  * @param monde un Monde
  */
  public String toString (Monde monde) {
    StringBuilder sb = new StringBuilder();

    sb.append("Monde : ").append(monde.nomDuMonde).append("\n");
    for (Entite e : monde.mapEntite.values()) {
			sb.append(e.toString());
		}
    return sb.toString();
  }
}
