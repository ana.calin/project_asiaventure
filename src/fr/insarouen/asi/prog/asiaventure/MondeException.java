package fr.insarouen.asi.prog.asiaventure;

/**
* Création de la classe MondeException fille de ASIAventureException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class MondeException extends ASIAventureException{

  // Constructeurs

  /**
  * Création d'un MondeException
  */
  public MondeException() {
  }

  /**
  * Création d'un MondeException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public MondeException(String msg) {
    super(msg);
  }
}
