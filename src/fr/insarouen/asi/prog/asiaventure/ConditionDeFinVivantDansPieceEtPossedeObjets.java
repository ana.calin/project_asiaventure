package fr.insarouen.asi.prog.asiaventure;

/**
* Condition de fin conjonction ConditionDeFinVivantDansPieceEtPossedeObjets
* Classe ayant le but de faire la conjonction des ConditionDeFin
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
import fr.insarouen.asi.prog.asiaventure.ConditionDeFin;
import fr.insarouen.asi.prog.asiaventure.ConditionDeFinVivantPossedeObjets;
import fr.insarouen.asi.prog.asiaventure.ConditionDeFinVivantDansPiece;
import fr.insarouen.asi.prog.asiaventure.ConditionDeFinConjonctionConditionDeFin;
import fr.insarouen.asi.prog.asiaventure.EtatDuJeu;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import java.util.*;


public class ConditionDeFinVivantDansPieceEtPossedeObjets extends ConditionDeFinConjonctionConditionDeFin{

  // Attributs
  private EtatDuJeu etatDuJeu;
  private ConditionDeFin conditionVivantDansPiece,conditionVivantPossedeObjets ;

  // Constructeurs

  /**
  * Création d'une ConditionDeFinVivantDansPieceEtPossedeObjets
  * @param etatConditionVerifiee l'etat du jeu
  * @param vivant le Vivant
  * @param piece la Pièce
  * @param objets les Objets
  */
  public ConditionDeFinVivantDansPieceEtPossedeObjets(EtatDuJeu etatConditionVerifiee, Vivant vivant, Piece piece, Objet... objets){
      super(etatConditionVerifiee);
      this.conditionVivantDansPiece= new ConditionDeFinVivantDansPiece(etatConditionVerifiee,vivant,piece);
      this.conditionVivantPossedeObjets =new ConditionDeFinVivantPossedeObjets(etatConditionVerifiee,vivant,objets);
  }
}
