package fr.insarouen.asi.prog.asiaventure;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;
import fr.insarouen.asi.prog.asiaventure.Simulateur;

import java.io.*;
import java.util.*;

/**
* Création de la classe main
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class Main {
  static Simulateur simulator;

  /**
  * Main
  * @param args les parametres qui peuvent etre entres via l'entrée standard
  */
  public static void main(String[] args) throws Throwable {
    Scanner choixUtilisateur = new Scanner(System.in);
    int a;
    String nomFichier;

    do {
      affichageMenu();

      switch (a=choixUtilisateur.nextInt()) {
        case 1 :
          jouer();
          break;
        case 2 :
          chargerDescription();
          break;
        case 3 :
          sauverPartieActuelle();
          break;
        case 4 :
          chargerPartie();
          break;
        case 5 :
          //simulator.stopperJeu(); // On ne le fait pas apparemment
          break;
      }
    } while (a!=5);
  }

  private static void affichageMenu() {
    System.out.println("--- Menu ---");
    System.out.println("1/ jouer");
    System.out.println("2/ charger un fichier de description");
    System.out.println("3/ sauver la partie actuelle");
    System.out.println("4/ charger une partie");
    System.out.println("5/ quitter");
  }

  private static String demanderInfoAlUtilisateur() {
    Scanner nomFichier = new Scanner(System.in);

    return nomFichier.next();
  }

  private static void jouer() throws Throwable {
    Scanner choixUtilisateur = new Scanner(System.in);
    String choix="non";
    EtatDuJeu etatDuGame;
    do {
      etatDuGame=simulator.executerUnTour();

      if (etatDuGame==EtatDuJeu.ENCOURS) {
        do {
          System.out.println("Souhaitez-vous rejouer ? (oui/non)");
          choix=choixUtilisateur.next();
        } while ((choix.equals("oui")==false) && (choix.equals("non")==false));
      }
      System.out.println(etatDuGame);
    } while (choix.equals("oui") && (etatDuGame==EtatDuJeu.ENCOURS));
  }

  private static void chargerDescription() throws IOException {
    String nomFichier;

    System.out.println("Donnez le nom du fichier de description que vous voulez charger");
    nomFichier=demanderInfoAlUtilisateur();
    try {
    	simulator = new Simulateur(new FileReader(nomFichier)); //simulator = new Simulateur(new FileReader(new File("nomFichier")));
      System.out.println("Le fichier de description a été chargé");
		} catch(NomDEntiteDejaUtiliseDansLeMondeException e) {
			e.printStackTrace();
		} catch(FileNotFoundException e) {
			System.out.println("Impossible de trouver le fichier : " + nomFichier);
		} catch(IOException e) {
			e.printStackTrace();
		}
  }

  private static void sauverPartieActuelle() throws IOException {
    String nomFichier;
    ObjectOutputStream oos = null;

    System.out.println("Donnez le nom du fichier dans lequel vous voulez sauvegarder la partie actuelle");
		try {

      nomFichier=demanderInfoAlUtilisateur();
			oos = new ObjectOutputStream(new FileOutputStream(nomFichier));
			simulator.enregistrer(oos);
      System.out.println("Votre partie a été enregistrée");
		} catch(IOException e) {
			e.printStackTrace();
		} finally {
			if (oos != null) {
        oos.close();
      }
    }
  }

  private static void chargerPartie() throws IOException {
    String nomFichier;
    ObjectInputStream ois = null;

    System.out.println("Donnez le nom du fichier contenant une partie que vous voulez charger");
    nomFichier=demanderInfoAlUtilisateur();
    try {
      ois = new ObjectInputStream(new FileInputStream(nomFichier));
      simulator = new Simulateur(ois);
      System.out.println("Votre partie a été chargée");
    } catch(IOException e) {
      System.out.println("Impossible de trouver le fichier : " + nomFichier);
    } catch(ClassNotFoundException e) {
      e.printStackTrace();
    } finally {
      if (ois != null) {
        ois.close();
      }
    }
  }
}
