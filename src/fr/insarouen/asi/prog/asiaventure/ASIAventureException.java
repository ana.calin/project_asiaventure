package fr.insarouen.asi.prog.asiaventure;

/**
* Création de la classe ASIAventureException fille de java.lang.Exception
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ASIAventureException extends java.lang.Exception {

  // Constructeurs

  /**
  * Création d'un ASIAventureException
  */
  public ASIAventureException() {
  }

  /**
  * Création d'un ASIAventureException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public ASIAventureException(String msg){
     super(msg);
  }
}
