package fr.insarouen.asi.prog.asiaventure;

/**
* EtatDuJeu est de type énuméré
* Enumeration des différents etats du jeu
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public enum EtatDuJeu {ECHEC, ENCOURS, SUCCES};
