package fr.insarouen.asi.prog.asiaventure;

import java.io.*;
import java.util.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.ConditionDeFin;

/**
  * Simulateur est la classe représentant le jeu
  * @author Ana CALIN et Damien TOOMEY
  * @version 1.0
  */
public class Simulateur {

  // Attributs

  /**
  * L'état du jeu.
  */
  private EtatDuJeu etatDuJeu;

  /**
  * Le monde du jeu.
  */
  private Monde monde;

  /**
  * Les pièces présentes dans le jeu.
  */
  private Map<String, Piece> pieces;

  /**
  * Les conditions dans le jeu.
  */
  private List<ConditionDeFin> cdf = new LinkedList<ConditionDeFin>();

  private JoueurHumain joueur;

  // Constructeurs

  /**
  * Constructeur Simulateur
  * @param monde le monde dont on veut creer le Simulateur
  * @param conditionDeFin les conditions de fin afin de creer un simulateur
  */
  public Simulateur(Monde monde, ConditionDeFin... conditionDeFin){
     this.monde = monde;
     for(int i = 0; i < conditionDeFin.length; i++) {
			this.ajouterConditionDeFin(conditionDeFin[i]);
		}
		this.etatDuJeu = EtatDuJeu.ENCOURS; // Damien this.

  }

  /**
  * Constructeur Simulateur
  * @param ois Le flux de données qui contient le monde du jeu
  * @throws IOException
  * @throws ClassNotFoundException Si la donnée lue n'est pas de type Monde
  */
  public Simulateur(ObjectInputStream ois) throws IOException, ClassNotFoundException {
    this.monde = (Monde)(ois.readObject());
    this.etatDuJeu = (EtatDuJeu)(ois.readObject());
    this.cdf = (List<ConditionDeFin>)(ois.readObject());
  }

  /**
  * Constructeur Simulateur
  * @param reader le flux de données qui contient
  * @throws IOException
  * @throws NomDEntiteDejaUtiliseDansLeMondeException Si le nom d'entité lu est déjà utilisé dans le monde
  */
  public Simulateur(Reader reader) throws IOException, NomDEntiteDejaUtiliseDansLeMondeException {
    this.etatDuJeu = EtatDuJeu.ENCOURS; // Damien
    this.pieces= new HashMap<String, Piece>();

    StreamTokenizer stream = new StreamTokenizer(reader); //Le reader est en octet (de bas niveau) donc on le passe en StreamTokenizer (en caractères) pour l'analyser
    stream.nextToken();

    while(stream.TT_EOF != stream.ttype) {
      switch(stream.sval) {
        case "Monde":
          setMonde(stream);
          break;
        case "Piece":
          setPiece(stream);
          break;
        case "PorteSerrure":
          setPorteSerrure(stream);
          break;
        case "Porte":
          setPorte(stream);
          break;
        case "Clef":
          setClef(stream);
          break;
        case "JoueurHumain":
          setJoueurHumain(stream);
          break;
        case "ConditionDeFinVivantDansPiece":
          setConditionDeFinVivantDansPiece(stream);
        default:
          break;
      }
      stream.nextToken();
    }
    reader.close();
  }

  // Méthodes privées

  private void setMonde(StreamTokenizer stream) throws IOException {
    stream.nextToken();
    this.monde = new Monde(stream.sval);
  }

  private void setPiece(StreamTokenizer stream) throws NomDEntiteDejaUtiliseDansLeMondeException, IOException {
    stream.nextToken();
    Piece piece = new Piece(stream.sval, this.getMonde());
    this.pieces.put(piece.getNom(),piece);
  }

  private void setPorteSerrure(StreamTokenizer stream) throws NomDEntiteDejaUtiliseDansLeMondeException, IOException {
    stream.nextToken();
    String nomPorte = new String(stream.sval);
    stream.nextToken();
    String pieceA = new String(stream.sval);
    stream.nextToken();
    String pieceB = new String(stream.sval);
    Porte porte = new Porte(nomPorte, this.getMonde(), new Serrure(this.getMonde()), pieces.get(pieceA), pieces.get(pieceB));
  }

  private void setPorte(StreamTokenizer stream) throws NomDEntiteDejaUtiliseDansLeMondeException, IOException {
    stream.nextToken();
    String nomPorte = new String(stream.sval);
    stream.nextToken();
    String pieceA = new String(stream.sval);
    stream.nextToken();
    String pieceB = new String(stream.sval);
    Porte porte = new Porte(nomPorte, this.getMonde(), pieces.get(pieceA), pieces.get(pieceB));
  }

  private void setClef(StreamTokenizer stream) throws NomDEntiteDejaUtiliseDansLeMondeException, IOException {
    stream.nextToken();
    String nomPorte = new String(stream.sval);
    stream.nextToken();
    String piece = new String(stream.sval);
    Porte porte = (Porte)this.getMonde().getEntite(nomPorte);
    Clef clef = porte.getSerrure().creerClef();
    pieces.get(piece).deposer(clef);
  }

  private void setJoueurHumain(StreamTokenizer stream) throws NomDEntiteDejaUtiliseDansLeMondeException, IOException {
    stream.nextToken();
    String nomJoueur = new String(stream.sval);
    stream.nextToken();
    int pdv = (int)stream.nval;
    stream.nextToken();
    int pdf = (int)stream.nval;
    stream.nextToken();
    String piece = new String(stream.sval);
    this.joueur = new JoueurHumain(nomJoueur, this.getMonde(), pdv, pdf, pieces.get(piece));
  }

  private void setConditionDeFinVivantDansPiece(StreamTokenizer stream) throws NomDEntiteDejaUtiliseDansLeMondeException, IOException{
    stream.nextToken();
    EtatDuJeu etat = EtatDuJeu.valueOf(stream.sval);
    stream.nextToken();
    stream.nextToken();
    String piece = new String(stream.sval);
    ConditionDeFinVivantDansPiece cdfv = new ConditionDeFinVivantDansPiece(etat, this.joueur,pieces.get(piece));
    this.ajouterConditionDeFin(cdfv);
  }

  // Méthodes

  /**
  * Retourne l'Etat du jeu.
  * @return EtatDuJeu l'Etat du jeu.
  */
  public EtatDuJeu getEtatDuJeu() {
    return this.etatDuJeu;
  }

  /**
  * Retourne le Monde du jeu.
  * @return Monde le Monde du jeu.
  */
  public Monde getMonde() {
    return this.monde;
  }

  /**
  * Effectue une sauvegarde du jeu, c'est-à-dire sérialise l'ensemble des données du jeu.
  * @param oos le flux de données qui contiendra les données du jeu actuel.
  * @throws IOException
  */
  public void enregistrer(ObjectOutputStream oos) throws IOException {
    oos.writeObject(this.monde);
    oos.writeObject(this.etatDuJeu);
    oos.writeObject(this.cdf);
  }

  /**
  * Ajout conditionDeFin au simulateur.
  * @param condition la condition que nous voulons ajouter
  */
  public void ajouterConditionDeFin(ConditionDeFin condition){
    this.cdf.add(condition);
  }

  /**
  * Ajout conditions De Fin au simulateur.
  * @param conditions la condition que nous voulons ajouter
  */
  public void ajouterConditionDeFin(Collection<ConditionDeFin> conditions){
    for (ConditionDeFin c : conditions){
      this.ajouterConditionDeFin(c);
    }
  }

  private void demandeOrdre(JoueurHumain jh){
    System.out.println("------------------------------------");
	  System.out.println("Situation joueur \n"+jh.toString());
    System.out.println("Etat du jeu : "+this.etatDuJeu);
    System.out.println("------------------------------------");
    System.out.println("Entrez un ordre :");
	  jh.setOrdre((new Scanner(System.in)).nextLine());
	  }

  /**
  * Executer un tour : afficher la situratin, demande de saisir un ordre, verification etat final .
  * @return EtatDuJeu l'etat apres avoir jouer un tour
  */
   public EtatDuJeu executerUnTour() throws Throwable{

      if(this.getEtatDuJeu() == EtatDuJeu.ENCOURS){

    	  for (Executable e : monde.getExecutables()) {
    			if(e instanceof JoueurHumain) {
    				demandeOrdre((JoueurHumain)e);
    			}
    		}

    		for (Executable e : monde.getExecutables()) {
    			e.executer();
    		}

    		for (ConditionDeFin c : this.cdf) {

    			if (c.verifierCondition() != EtatDuJeu.ENCOURS) {
    				this.etatDuJeu = c.getEtatConditionVerifiee();
    				return this.etatDuJeu;
    			}
    		}

    	}
    	return this.etatDuJeu;
   }

   /**
    * Executer N tours : afficher la situratin, demande de saisir un ordre, verification etat final pour n tours .
    * @return EtatDuJeu l'etat apres avoir jouer un tour
    */
   public EtatDuJeu executerNTours(int n) throws Throwable {
		while (n > 0) {
			executerUnTour();
			n--;
		}
		return getEtatDuJeu();
	}

	/**
	 * Executer des tours de jeux jusqu'à la fin,jusqu'au changement du ENCOURS.
	 *
	 * @return EtatDuJeu après l'execution.
	 */
	public EtatDuJeu executerJusquALaFin() throws Throwable {
		while(executerUnTour() == EtatDuJeu.ENCOURS);
		return this.etatDuJeu;
	}

  /**
  * Retourne l'ensemble des informations du jeu.
  * @return String l'ensemble des informations du jeu sous forme d'une chaîne de caractères.
  */
  public String toString() {
    StringBuilder s=new StringBuilder("Le jeu : \n");
    s.append("Nom du monde : ");
    s.append(this.monde.getNom()).append("\n");
    s.append("--------------------------------").append("\n");

    for(Piece piece : this.pieces.values()) {
      s.append("Nom de la pièce : ");
      s.append(piece.toString()).append("\n");
      s.append("--------------------------------").append("\n");
    }

    return s.toString(); // toString de StringBuilder
  }
}
