package fr.insarouen.asi.prog.asiaventure;

/**
* Création de la classe EntiteDejaDansUnAutreMondeException fille de MondeException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class EntiteDejaDansUnAutreMondeException extends MondeException{

  // Constructeurs

  /**
  * Création d'un EntiteDejaDansUnAutreMondeException
  */
  public EntiteDejaDansUnAutreMondeException() {
  }

  /**
  * Création d'un EntiteDejaDansUnAutreMondeException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public EntiteDejaDansUnAutreMondeException(String msg){
    super(msg);
  }
}
