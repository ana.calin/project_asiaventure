package fr.insarouen.asi.prog.asiaventure;

/**
* Condition de fin
* Classe ayant le but de verifier si une condition est remplie
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/

import fr.insarouen.asi.prog.asiaventure.EtatDuJeu;

public abstract class ConditionDeFin extends Object implements java.io.Serializable{

  // Attribut
  private EtatDuJeu etatDuJeu;

  // Constructeurs

  /**
  * Création d'une ConditionDeFin
  * @param etatDuJeu l'etat du jeu
  */
  public ConditionDeFin(EtatDuJeu etatDuJeu){
    this.etatDuJeu=etatDuJeu;
  }

  // Méthodes

  /**
  * Acces à l'etat du jeu
  * @return EtatDuJeu
  */
  public EtatDuJeu getEtatConditionVerifiee(){
      return this.etatDuJeu;
  }

  /**
  * Verification d'etat du jeu
  * @return EtatDuJeu
  */
  public abstract EtatDuJeu verifierCondition();

}
