package fr.insarouen.asi.prog.asiaventure;

import fr.insarouen.asi.prog.asiaventure.MondeException;

/**
* Création de la classe NomDEntiteDejaUtiliseDansLeMondeException fille de MondeException
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class NomDEntiteDejaUtiliseDansLeMondeException extends MondeException {

  // Constructeurs

  /**
  * Création d'un NomDEntiteDejaUtiliseDansLeMondeException
  */
  public NomDEntiteDejaUtiliseDansLeMondeException() {
  }

  /**
  * Création d'un NomDEntiteDejaUtiliseDansLeMondeException
  * @param msg Message qu'on souhaite afficher lors de l'exception
  */
  public NomDEntiteDejaUtiliseDansLeMondeException(String msg) {
    super(msg);
  }
}
