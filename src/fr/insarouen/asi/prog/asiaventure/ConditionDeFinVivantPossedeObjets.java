package fr.insarouen.asi.prog.asiaventure;

/**
* Condition de fin conjonction ConditionDeFin
* Classe ayant le but de faire la conjonction des ConditionDeFin
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
import fr.insarouen.asi.prog.asiaventure.ConditionDeFin;
import fr.insarouen.asi.prog.asiaventure.EtatDuJeu;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import java.util.*;

public class ConditionDeFinVivantPossedeObjets extends ConditionDeFin {

  // Attributs

  private EtatDuJeu etatDuJeu;
  private Vivant vivant;
  private ArrayList<Objet> mapObjet;

  // Constructeurs

  /**
  * Création d'une ConditionDeFinVivantPossedeObjets
  * @param etatConditionVerifiee l'etat du jeu
  * @param vivant le Vivant
  * @param objets les objets que le Vivant possède
  */
  public ConditionDeFinVivantPossedeObjets(EtatDuJeu etatConditionVerifiee,Vivant vivant,Objet[] objets){
    super(etatConditionVerifiee);
    this.vivant=vivant;
    for(Objet o : objets) {
			this.mapObjet.add(o);
		}
  }

  // Méthodes
  
  /**
  * Verification d'etat du jeu
  * @return EtatDuJeu
  */
  public EtatDuJeu verifierCondition(){
    for(Objet o : mapObjet ) {
			if (!this.vivant.possede(o)){
        return EtatDuJeu.ENCOURS;
      }
		}
    return getEtatConditionVerifiee();
  }
}
