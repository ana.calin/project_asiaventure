package fr.insarouen.asi.prog;

import  fr.insarouen.asi.prog.asiaventure.elements.Entite;
import  fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

/**
* Création de la classe ClassMainTestEntite
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ClassMainTestEntite {

  /**
  * Main pour ClassMainTestEntite
  * @param args les parametres qui peuvent etre entres via l'entree standard
  */
  public static void main(String[] args){

    // Test Creation Entite / getNom / getMonde
    try{
    Monde mondeTest = new Monde ("MondeTest");
    Entite entiteTest= new Entite("EntiteTest", mondeTest){};

    if ((entiteTest.getNom().equals("EntiteTest")) && (entiteTest.getMonde().getNom().equals("MondeTest"))){
       System.out.println("TEST Creation/getNom/getMonde Entite ----- OK");
    }

    // Test Equals
    Monde mMonde = new Monde ("MondeTest");
    Entite mEntite= new Entite("EntiteTest", mMonde){};
    Monde dMonde = new Monde ("MondeTestDif");
    Entite dEntite= new Entite("EntiteTestDif", dMonde){};

    if (entiteTest.equals(mEntite)) {
      System.out.println("TEST Equals avec les memes entites ----- OK");
    }

    if (!(entiteTest.equals(dEntite))) {
      System.out.println("TEST Equals avec entites differentes ----- OK");
    }

    // Test deux Entité avec le même nom dans le même Monde
    Monde planete = new Monde ("Terre");
    Entite entite1= new Entite("EntiteTest", planete){};
    Entite entite2= new Entite("EntiteTest", planete){};

  } catch (NomDEntiteDejaUtiliseDansLeMondeException e) {
      e.printStackTrace();
  }
}
}
