package fr.insarouen.asi.prog;

import  fr.insarouen.asi.prog.asiaventure.Monde;
import  fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.EntiteDejaDansUnAutreMondeException;

/**
* Création de la classe ClassMainTestMonde
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class ClassMainTestMonde {

  /**
  * Main pour ClassMainTestMonde
  * @param args les parametres qui peuvent etre entres via l'entree standard
  */
  public static void main(String[] args) {
try {
    Monde m1 = new Monde("toto");
		Monde m2 = new Monde("tata");

		Entite e1 = new Entite("e1",m1){};
		Entite e2 = new Entite("e2",m1){};
		Entite e3 = new Entite("e3",m1){};

    Entite e4 = new Entite("e4",m2){};
		Entite e5 = new Entite("e5",m2){};
		Entite e6 = new Entite("e6",m2){};
		Entite e7 = new Entite("e7",m2){};
		Entite e8 = new Entite("e8",m2){};

		System.out.println(e1.toString());
		System.out.println(e2.toString());
		System.out.println(e3.toString());

    System.out.println("");

		System.out.println(m1.toString(m1));

    // Test ajouter l'Entité e4 dans m1 alors que cette Entité existe déjà dans m2
    // System.out.println("Test ajouter l'Entité e4 dans m1 alors que cette Entité existe déjà dans m2");
		// m1.ajouter(e4);
		// System.out.println(m1.toString(m1));

    System.out.println(e4.getNom());
    System.out.println(m2.getEntite(e4.getNom()));

    System.out.println("");

    // Test création de deux Entité avec le même nom dans le même Monde
    // System.out.println("Test création de deux Entité avec le même nom dans le même Monde");
    // Monde planete = new Monde ("Terre");
    // Entite entite1= new Entite("EntiteTest", planete){};
    // Entite entite2= new Entite("EntiteTest", planete){};

    // Test ajout de deux Entité avec le même nom dans le même Monde
    System.out.println("Test ajout de deux Entité avec le même nom dans le même Monde");
    Monde planete = new Monde ("Terre");
    Entite entite1= new Entite("EntiteTest", planete){};
    planete.ajouter(entite1);

  } catch (NomDEntiteDejaUtiliseDansLeMondeException e) {
    e.printStackTrace();
  }
  catch (EntiteDejaDansUnAutreMondeException e) {
    e.printStackTrace();
  }

  }
}
