package fr.insarouen.asi.prog.asiaventure;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import fr.insarouen.asi.prog.asiaventure.Simulateur;
import java.util.*;
import java.io.*;

/**
* Création de la classe TestSimulateur
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestSimulateur {

  // @Before
  // public void avantTest() throws IOException,NomDEntiteDejaUtiliseDansLeMondeException{
  //   ObjectInputStream ois = new ObjectInputStream(new FileInputStream("fichierTest.txt"));
  //   Simulateur sim = new Simulateur(ois);
  // }

  @Test
  public void TestSimulateur() throws IOException,NomDEntiteDejaUtiliseDansLeMondeException{
      Simulateur sim1 = null;
      Reader reader = new FileReader("fichierTest");

      sim1 = new Simulateur(reader);

       if(reader != null) {
         reader.close();
       }

      assertThat(sim1.getMonde().getNom().equals("Le Monde impitoyable d’ASI"), is(true));
      //System.out.println(sim1.toString());
  }
}
