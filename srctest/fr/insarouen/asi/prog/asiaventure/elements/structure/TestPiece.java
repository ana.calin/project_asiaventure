package fr.insarouen.asi.prog.asiaventure.elements.structure;

// import fr.insarouen.asi.prog.asiaventure.elements.structure.ElementStructurel;
// import fr.insarouen.asi.prog.asiaventure.Monde;
// import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
// import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
// import fr.insarouen.asi.prog.asiaventure.elements.Entite;
 import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;
// import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
//
// import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
// import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;
// import fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;
// import fr.insarouen.asi.prog.asiaventure.elements.structure.Porte;

import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;

import static org.junit.Assert.*;
import org.hamcrest.core.IsEqual;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import java.util.*;

/**
* Création de la classe TestPiedDeBiche
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestPiece {

// Tout ce qui concerne Objet

    // Test deposer, contientObjet(Objet), contientObjet(nomObjet)
    @Test
    public void Test_deposer() throws NomDEntiteDejaUtiliseDansLeMondeException {
      Monde m = new Monde("toto");
      Piece piece = new Piece("cuisine", m);
      PiedDeBiche crowbar = new PiedDeBiche("crowbar", m);

      piece.deposer(crowbar);
      assertThat(piece.contientObjet(crowbar), is(true));
      assertThat(piece.contientObjet(crowbar.getNom()), is(true));
    }

    // Test retirer(nomObjet)
    @Test
    public void Test_retirernomObjet() throws NomDEntiteDejaUtiliseDansLeMondeException,ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
      Monde m = new Monde("toto");
      Piece piece = new Piece("cuisine", m);
      PiedDeBiche crowbar = new PiedDeBiche("crowbar", m);

      piece.deposer(crowbar);
      piece.retirer(crowbar.getNom());
      assertThat(piece.contientObjet(crowbar.getNom()), is(false));
    }

    // Test retirer(Objet)
    @Test
    public void Test_retirerObjet() throws NomDEntiteDejaUtiliseDansLeMondeException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
      Monde m = new Monde("toto");
      Piece piece = new Piece("cuisine", m);
      PiedDeBiche crowbar = new PiedDeBiche("crowbar", m);

      piece.deposer(crowbar);
      piece.retirer(crowbar);
      assertThat(piece.contientObjet(crowbar.getNom()), is(false));
    }

    // Test getObjets()
    @Test
    public void Test_getObjets() throws NomDEntiteDejaUtiliseDansLeMondeException {
      Monde m = new Monde("toto");
      Piece piece = new Piece("cuisine", m);
      PiedDeBiche crowbar = new PiedDeBiche("crowbar", m);

      piece.deposer(crowbar);
      Collection<Objet> CollectionOjetsDeLaPiece = piece.getObjets();

      assertThat(CollectionOjetsDeLaPiece.contains(crowbar), is(true));

      PiedDeBiche crowbar2 = new PiedDeBiche("crowbar2", m);
      PiedDeBiche crowbar3 = new PiedDeBiche("crowbar3", m);
      piece.deposer(crowbar2);
      piece.deposer(crowbar3);

      PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[3];
      tabPiedDeBiche[0] = crowbar;
      tabPiedDeBiche[1] = crowbar2;
      tabPiedDeBiche[2] = crowbar3;

      for (int i = 0; i<tabPiedDeBiche.length; i++) {
        assertThat(CollectionOjetsDeLaPiece.contains(tabPiedDeBiche[i]), is(true));
      }
    }
// Tout ce qui concerne porte

     // Test aLaPorte()
     @Test
     public void Test_aLaPorte() throws NomDEntiteDejaUtiliseDansLeMondeException{
       Monde mondejava = new Monde("titi");
       Piece piece1 = new Piece("piece1", mondejava) ;
		   Piece piece2 = new Piece("piece2", mondejava);
		   Piece piece3 = new Piece("piece3", mondejava) ;
		   Porte p1 = new Porte("porte1", mondejava, piece1, piece2);
		   Porte p2 = new Porte("porte2", mondejava, piece2, piece3);
       piece1.addPorte(p1);
       piece2.addPorte(p1);
       piece2.addPorte(p2);
       piece3.addPorte(p2);
		   assertThat(piece1.aLaPorte(p1), IsEqual.equalTo(true));
		   assertThat(piece2.aLaPorte(p1), IsEqual.equalTo(true));
		   assertThat(piece3.aLaPorte(p1), IsEqual.equalTo(false));

		   assertThat(piece1.aLaPorte(p2), IsEqual.equalTo(false));
		   assertThat(piece2.aLaPorte(p2), IsEqual.equalTo(true));
		   assertThat(piece3.aLaPorte(p2), IsEqual.equalTo(true));

		   assertThat(piece1.aLaPorte("porte1"), IsEqual.equalTo(true));
		   assertThat(piece2.aLaPorte("porte1"), IsEqual.equalTo(true));
		   assertThat(piece3.aLaPorte("porte1"), IsEqual.equalTo(false));

		   assertThat(piece1.aLaPorte("porte2"), IsEqual.equalTo(false));
		   assertThat(piece2.aLaPorte("porte2"), IsEqual.equalTo(true));
		   assertThat(piece3.aLaPorte("porte2"), IsEqual.equalTo(true));
     }

      // Test getPorte()
     	@Test
     	public void Test_getPorte() throws NomDEntiteDejaUtiliseDansLeMondeException {
        Monde m = new Monde("titi");
     		Piece piece1 = new Piece("piece1", m) ;
     		Piece piece2 = new Piece("piece2", m) ;
     		Porte p1 = new Porte("porte1", m, piece1, piece2) ;
        piece1.addPorte(p1);
        piece2.addPorte(p1);
        assertThat(piece1.aLaPorte("porte1"), IsEqual.equalTo(true));
     		assertThat(piece2.aLaPorte(p1), IsEqual.equalTo(true));

     		assertThat(piece1.getPorte("porte1"), IsEqual.equalTo(p1));
      	assertThat(piece1.getPorte("porte2")==null, is(true));
     	}

// Tout ce qui concerne Vivant

    // Test entrer(Vivant)
    @Test
    public void Test_entrerVivant() throws NomDEntiteDejaUtiliseDansLeMondeException {
      Monde m1 = new Monde("Venus");
      int pointVie = 10;
      int pointForce = 10;
      Piece piece = new Piece("cuisine", m1);
      PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[3];
      PiedDeBiche piedDeBiche1 = new PiedDeBiche("crowbar1", m1);
      PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", m1);
      PiedDeBiche piedDeBiche3 = new PiedDeBiche("crowbar3", m1);

      tabPiedDeBiche[0]=piedDeBiche1;
      tabPiedDeBiche[1]=piedDeBiche2;
      tabPiedDeBiche[2]=piedDeBiche3;

      Vivant vivant1 = new Vivant("homme", m1, pointVie, pointForce, piece, tabPiedDeBiche){};
      Vivant vivant2 = new Vivant("homme2", m1, pointVie, pointForce, piece, tabPiedDeBiche){};

      piece.entrer(vivant1);
      piece.entrer(vivant2);
      assertThat(((piece.contientVivant(vivant1)) && (piece.contientVivant(vivant2))), is(true));
    }

    // Test sortir(nomVivant)
    @Test
    public void Test_sortirnomVivant() throws NomDEntiteDejaUtiliseDansLeMondeException, VivantAbsentDeLaPieceException {
      Monde m1 = new Monde("Venus");
      int pointVie = 10;
      int pointForce = 10;
      Piece piece = new Piece("cuisine", m1);
      PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[3];
      PiedDeBiche piedDeBiche1 = new PiedDeBiche("crowbar1", m1);
      PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", m1);
      PiedDeBiche piedDeBiche3 = new PiedDeBiche("crowbar3", m1);

      tabPiedDeBiche[0]=piedDeBiche1;
      tabPiedDeBiche[1]=piedDeBiche2;
      tabPiedDeBiche[2]=piedDeBiche3;

      Vivant vivant1 = new Vivant("homme", m1, pointVie, pointForce, piece, tabPiedDeBiche){};
      piece.entrer(vivant1);
      piece.sortir(vivant1.getNom());
      assertThat(piece.contientVivant(vivant1.getNom()), is(false));
    }

    //Test sortir(Vivant)
    @Test
    public void Test_sortirVivant() throws NomDEntiteDejaUtiliseDansLeMondeException, VivantAbsentDeLaPieceException {
      Monde m1 = new Monde("Venus");
      int pointVie = 10;
      int pointForce = 10;
      Piece piece = new Piece("cuisine", m1);
      PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[3];
      PiedDeBiche piedDeBiche1 = new PiedDeBiche("crowbar1", m1);
      PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", m1);
      PiedDeBiche piedDeBiche3 = new PiedDeBiche("crowbar3", m1);

      tabPiedDeBiche[0]=piedDeBiche1;
      tabPiedDeBiche[1]=piedDeBiche2;
      tabPiedDeBiche[2]=piedDeBiche3;

      Vivant vivant1 = new Vivant("homme", m1, pointVie, pointForce, piece, tabPiedDeBiche){};

      piece.entrer(vivant1);
      piece.sortir(vivant1);
      assertThat(piece.contientVivant(vivant1), is(false));
    }

// Test des exceptions

    // Test retirer(nomPiedDeBiche) non présent de la Pièce
    @Test(expected=ObjetAbsentDeLaPieceException.class)
    public void retirernomPiedDeBiche() throws NomDEntiteDejaUtiliseDansLeMondeException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
      Monde planete = new Monde("Terre");
      Piece livingRoom = new Piece("livingRoom", planete);
      PiedDeBiche crowbar = new PiedDeBiche("crowbar", planete);
      livingRoom.retirer(crowbar.getNom());
    }

    // Test retirer(PiedDeBiche) non présent de la Pièce
    @Test(expected=ObjetAbsentDeLaPieceException.class)
    public void retirerPiedDeBiche() throws NomDEntiteDejaUtiliseDansLeMondeException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
      Monde planete = new Monde("Terre");
      Piece livingRoom = new Piece("livingRoom", planete);
      PiedDeBiche crowbar = new PiedDeBiche("crowbar", planete);
      livingRoom.retirer(crowbar);
    }

    //Test retirer(nomObjet) non déplaçable
    @Test(expected=ObjetNonDeplacableException.class)
    public void retirernomObjetNonDeplacable() throws NomDEntiteDejaUtiliseDansLeMondeException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {

      Monde planete = new Monde("Terre");
      Piece livingRoom = new Piece("livingRoom", planete);
      Objet objetQuelconque = new Objet("aglo", planete) {
        public boolean estDeplacable() {
          return false;
        }
      };

      livingRoom.deposer(objetQuelconque);
      livingRoom.retirer(objetQuelconque.getNom());
    }

    // Test retirer(Objet) non déplaçable
    @Test(expected=ObjetNonDeplacableException.class)
    public void retirerObjetNonDeplacable() throws NomDEntiteDejaUtiliseDansLeMondeException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {

      Monde planete = new Monde("Terre");
      Piece livingRoom = new Piece("livingRoom", planete);
      Objet objetQuelconque = new Objet("aglo", planete) {
        public boolean estDeplacable() {
          return false;
        }
      };

      livingRoom.deposer(objetQuelconque);
      livingRoom.retirer(objetQuelconque);
    }

    // Test sorir(nomVivant) non présent dans la Pièce
    @Test(expected=VivantAbsentDeLaPieceException.class)
    public void Test_sortirnomVivant_nonPresentDansPiece() throws NomDEntiteDejaUtiliseDansLeMondeException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException, VivantAbsentDeLaPieceException {

      Monde planete = new Monde("Venus");
      int pointVie = 10;
      int pointForce = 10;
      Piece piece = new Piece("cuisine", planete);
      PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[3];
      PiedDeBiche piedDeBiche1 = new PiedDeBiche("crowbar1", planete);
      PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", planete);
      PiedDeBiche piedDeBiche3 = new PiedDeBiche("crowbar3", planete);

      tabPiedDeBiche[0]=piedDeBiche1;
      tabPiedDeBiche[1]=piedDeBiche2;
      tabPiedDeBiche[2]=piedDeBiche3;

      Vivant jamie = new Vivant("Jamie", planete, pointVie, pointForce, piece, tabPiedDeBiche){};
      piece.sortir(jamie.getNom());

      // Pour faire sortir le vivant alors qu'il n'est plus dans la Piece
      piece.sortir(jamie.getNom());
    }

    // Test retirer(Vivant) non présent dans la Pièce
    @Test(expected=VivantAbsentDeLaPieceException.class)
    public void Test_sortirVivant_nonPresentDansPiece() throws NomDEntiteDejaUtiliseDansLeMondeException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException, VivantAbsentDeLaPieceException {

      Monde planete = new Monde("Venus");
      int pointVie = 10;
      int pointForce = 10;
      Piece piece = new Piece("cuisine", planete);
      PiedDeBiche[] tabPiedDeBiche = new PiedDeBiche[3];
      PiedDeBiche piedDeBiche1 = new PiedDeBiche("crowbar1", planete);
      PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", planete);
      PiedDeBiche piedDeBiche3 = new PiedDeBiche("crowbar3", planete);

      tabPiedDeBiche[0]=piedDeBiche1;
      tabPiedDeBiche[1]=piedDeBiche2;
      tabPiedDeBiche[2]=piedDeBiche3;

      Vivant jamie = new Vivant("Jamie", planete, pointVie, pointForce, piece, tabPiedDeBiche){};
      piece.sortir(jamie);

      // Pour faire sortir le vivant alors qu'il n'est plus dans la Piece
      piece.sortir(jamie);
    }
}
