package fr.insarouen.asi.prog.asiaventure.elements.structure;

import fr.insarouen.asi.prog.asiaventure.elements.Activable;
import fr.insarouen.asi.prog.asiaventure.elements.structure.ElementStructurel;
import fr.insarouen.asi.prog.asiaventure.elements.Etat;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Serrure;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleAvecObjetException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationException;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import java.util.*;

/**
* Création de la classe TestPiedDeBiche
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestPorte {
  private Monde monde, monde2;
  private String nom, nom2;
  private Piece pieceA, pieceA2;
  private Piece pieceB, pieceB2;
  private Porte porte, porte2;
  private Serrure serrure2;

    @Before
    public void Test_avantTest() throws ActivationImpossibleException, NomDEntiteDejaUtiliseDansLeMondeException {
      this.monde = new Monde("Terre");
      this.nom = new String("Porte1");
      this.pieceA = new Piece("cuisine", this.monde);
      this.pieceB = new Piece("salon",this.monde);
      this.porte = new Porte(this.nom, this.monde, this.pieceA, this.pieceB);

      this.monde2 = new Monde("Terre2");
      this.serrure2 = new Serrure(this.monde2);
      this.nom2 = new String("Porte2");
      this.pieceA2 = new Piece("cuisine2", this.monde2);
      this.pieceB2 = new Piece("salon2",this.monde2);
      this.porte2 = new Porte(this.nom2, this.monde2, this.serrure2, this.pieceA2, this.pieceB2);
    }

    // Tests constructeurs
    @Test
    public void Test_constructeur1() {
      assertThat(this.porte.getNom().equals("Porte1"), is(true));
      assertThat(this.porte.getMonde().getNom().equals("Terre"), is(true));
      assertThat(this.porte.getPieceAutreCote(this.pieceA).equals(this.pieceB), is(true));
      assertThat(this.porte.getPieceAutreCote(this.pieceB).equals(this.pieceA), is(true));
      assertThat(this.pieceA.aLaPorte(this.porte.getNom()), is(true));
      assertThat(this.pieceB.aLaPorte(this.porte.getNom()), is(true));
    }

    @Test
    public void Test_constructeur2() {
      assertThat(this.porte2.getNom().equals("Porte2"), is(true));
      assertThat(this.porte2.getMonde().getNom().equals("Terre2"), is(true));
      assertThat(this.porte2.getPieceAutreCote(this.pieceA2).equals(this.pieceB2), is(true));
      assertThat(this.porte2.getPieceAutreCote(this.pieceB2).equals(this.pieceA2), is(true));
      assertThat(this.serrure2.getNom().equals("serrure1"), is(true));
      assertThat(this.pieceA2.aLaPorte(this.porte2.getNom()), is(true));
      assertThat(this.pieceB2.aLaPorte(this.porte2.getNom()), is(true));
    }

    // Tests Méthodes
    // Test getEtat
    @Test
    public void Test_getEtat() {
      assertThat(this.porte.getEtat()==Etat.OUVERT, is(true));
    }

    // Test activer
    @Test
    public void Test_activer() throws ActivationImpossibleException {
      this.porte.activer();
      assertThat(this.porte.getEtat()==Etat.FERME, is(true));
    }

    // Test getPieceAutreCote
    @Test
    public void Test_getPieceAutreCote() {
      assertThat(this.porte.getPieceAutreCote(pieceA).equals(pieceB), is(true));
      assertThat(this.porte.getPieceAutreCote(pieceB).equals(pieceA), is(true));
    }

    // Test activerAvec
    @Test
    public void Test_activerAvec() throws NomDEntiteDejaUtiliseDansLeMondeException, ActivationImpossibleAvecObjetException, ActivationImpossibleException {
      PiedDeBiche piedDeBiche = new PiedDeBiche("crowbar", this.monde);
      PiedDeBiche piedDeBiche2 = new PiedDeBiche("crowbar2", this.monde2);

      assertThat(this.porte.getEtat()==Etat.OUVERT, is(true)); // car la porte est FERME de base

      this.porte.activer();
      assertThat(this.porte.getEtat()==Etat.FERME, is(true));

      this.porte.activerAvec(piedDeBiche);

      assertThat(this.porte.getEtat()==Etat.CASSE, is(true));

      assertThat(this.porte2.getEtat()==Etat.OUVERT, is(true));

      Clef clef = this.serrure2.creerClef();

      this.porte2.activer();
      assertThat(this.porte2.getEtat()==Etat.FERME, is(true));

      this.porte2.activerAvec(clef);
      assertThat(this.porte2.getEtat()==Etat.VERROUILLE, is(true));

      this.porte2.activerAvec(clef);
      assertThat(this.porte2.getEtat()==Etat.OUVERT, is(true));

      this.porte2.activerAvec(clef);
      assertThat(this.porte2.getEtat()==Etat.VERROUILLE, is(true));

      this.porte2.activerAvec(piedDeBiche2);
      assertThat(this.porte2.getEtat()==Etat.CASSE, is(true));
    }

    //Test activer avec exception
    @Test(expected=ActivationImpossibleAvecObjetException.class)
    public void Test_activerAvec2() throws NomDEntiteDejaUtiliseDansLeMondeException, ActivationImpossibleAvecObjetException, ActivationImpossibleException {
      Objet objetAuHasard = new Objet("nomObjet", this.monde){public boolean estDeplacable() {return false;}};

      this.porte2.activerAvec(objetAuHasard); // Provoque ActivationImpossibleAvecObjetException
    }

    //Test activer avec exception
    @Test(expected=ActivationImpossibleException.class)
    public void Test_activerAvec3() throws NomDEntiteDejaUtiliseDansLeMondeException, ActivationImpossibleAvecObjetException, ActivationImpossibleException {
      Clef clef = this.serrure2.creerClef();

      this.porte.activerAvec(clef);
    }

    //Test activableAvec
    @Test
    public void Test_activableAvec() throws NomDEntiteDejaUtiliseDansLeMondeException {
      Clef clef = this.serrure2.creerClef();
      assertThat(this.porte2.activableAvec(clef), is(true));

      PiedDeBiche piedDeBiche = new PiedDeBiche("crowbar", this.monde2);
      assertThat(this.porte2.activableAvec(piedDeBiche), is(true));

      assertThat(this.porte.activableAvec(piedDeBiche), is(true));

      assertThat(this.porte.activableAvec(clef), is(false));
    }
}
