package fr.insarouen.asi.prog.asiaventure.elements.structure;

import fr.insarouen.asi.prog.asiaventure.elements.structure.ElementStructurel;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;

import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;

import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
* Création de la classe TestElementStructurel
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestElementStructurel {

    // Test constructeur ElementStructurel(String nom, Monde monde)
    @Test
    public void Test_constructeur_ElementStructurel() throws NomDEntiteDejaUtiliseDansLeMondeException {
      Monde planete = new Monde("Terre");
      ElementStructurel elStruct = new ElementStructurel("yoyo", planete){};

      assertThat(elStruct.getNom(), IsEqual.equalTo("yoyo"));
      assertThat(elStruct.getMonde(), IsEqual.equalTo(planete));
    }

// Test des exceptions

    // Test constructeur ElementStructurel(String nom, Monde monde) avec NomDEntiteDejaUtiliseDansLeMondeException
    @Test(expected=NomDEntiteDejaUtiliseDansLeMondeException.class)
    public void Test_constructeur_ElementStructurel_exception() throws NomDEntiteDejaUtiliseDansLeMondeException {
      Monde planete = new Monde("Terre");
      ElementStructurel elStruct = new ElementStructurel("yoyo", planete){};

      ElementStructurel elStruct2 = new ElementStructurel("yoyo", planete){};
    }
}
