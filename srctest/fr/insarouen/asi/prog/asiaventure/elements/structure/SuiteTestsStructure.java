package fr.insarouen.asi.prog.asiaventure.elements.structure;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
* Création SuiteTestsStructure
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
@RunWith(Suite.class)
@SuiteClasses({
  TestPiece.class,
  TestElementStructurel.class,
  TestPorte.class
})
public class SuiteTestsStructure {}
