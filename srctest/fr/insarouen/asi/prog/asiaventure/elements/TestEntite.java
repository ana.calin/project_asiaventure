package fr.insarouen.asi.prog.asiaventure.elements;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.EntiteDejaDansUnAutreMondeException;

/**
* Création de la classe TestEntite
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestEntite {
  public Monde mondeJava1, mondeJava2;
  public Entite entite1;

  @Before
  public void avantTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
    this.mondeJava1 = new Monde("mondeJava1");
    this.mondeJava2 = new Monde("mondeJava2");
    this.entite1 = new Entite("nomDeLEntite1", mondeJava1){};
  }

  @Test
  public void test_Constructeur() {
    assertThat(this.entite1.getNom(), is("nomDeLEntite1"));
    assertThat(this.entite1.getMonde(), is(mondeJava1));
    assertThat(this.mondeJava1.getEntite("nomDeLEntite1"), is(entite1));
  }

  @Test(expected=NomDEntiteDejaUtiliseDansLeMondeException.class)
  public void test_ExceptionConstructeur() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Entite entite2 = new Entite("nomDeLEntite1",this.mondeJava1){};
  }

  @Test
  public void test_GetNom() {
    assertThat(this.entite1.getNom(), is("nomDeLEntite1"));
  }

  @Test
  public void test_GetMonde() {
    assertThat(this.entite1.getMonde(), is(mondeJava1));
  }

  @Test
  public void test_Equals() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Entite entite2 = entite1;
    Entite entite3 = new Entite("nomDeLEntite3", mondeJava1){};
    assertThat(entite1.equals(entite2), is(true));
    assertThat(entite2.equals(entite3), is(false));
  }

}
