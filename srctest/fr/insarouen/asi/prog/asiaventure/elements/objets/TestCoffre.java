package fr.insarouen.asi.prog.asiaventure.elements.objets;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
* Création de la classe TestObjet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestCoffre {


  @Test
  public void estDeplacableTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Monde m = new Monde("Terre");
    Coffre coffre = new Coffre("nomTest",m);
    assertThat(coffre.estDeplacable(), is(false));
  }

  @Test
  public void activableAvecTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Monde m = new Monde("Terre");
    Coffre coffre = new Coffre("nomTest",m);
    Objet obj = new Objet("obj",m){public boolean estDeplacable() {return false;}};
    assertThat(coffre.activableAvec(obj), is(false));
  }

  @Test
  public void activerTest() throws NomDEntiteDejaUtiliseDansLeMondeException,ActivationException {
    Monde m = new Monde("Terre");
    Coffre coffre = new Coffre("nomTest",m);
    coffre.activer();
    assertThat(coffre.getEtat()==Etat.OUVERT, is(true));
    coffre.activer();
    assertThat(coffre.getEtat()==Etat.FERME, is(true));
  }

  @Test(expected=ActivationImpossibleAvecObjetException.class)
  public void testActiverAvecObjetAvecException() throws NomDEntiteDejaUtiliseDansLeMondeException, ActivationException {
    Monde m = new Monde("Terre");
		Coffre coffre = new Coffre("c", m);
		Objet objet	= new Objet("objet", m) {
			public boolean estDeplacable() {return true;}
		};
		assertThat(coffre.getEtat(), IsEqual.equalTo(Etat.FERME));
		coffre.activerAvec(objet);
	}

}
