package fr.insarouen.asi.prog.asiaventure.elements.objets;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
* Création de la classe TestObjet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
@RunWith(Suite.class)
@SuiteClasses({
  TestObjet.class,
  TestPiedDeBiche.class,
  TestCoffre.class
})
public class SuiteTestsObjets {}
