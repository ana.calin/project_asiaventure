package fr.insarouen.asi.prog.asiaventure.elements.objets;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
* Création de la classe TestObjet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestObjet {

  @Test
  public void estDeplacableTestFaux() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Monde m = new Monde("Terre");
    Objet o = new Objet("o", m){public boolean estDeplacable() {return false;}};
    assertThat(o.estDeplacable(), is(false));
  }

  @Test
  public void estDeplacableTestVrai() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Monde m = new Monde("Mars");
		Objet o = new Objet("o", m){public boolean estDeplacable() {return true;}};
    assertThat(o.estDeplacable(), is(true));
  }

  @Test(expected=NomDEntiteDejaUtiliseDansLeMondeException.class)
  public void testExceptionConstructeurObjet() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Monde m = new Monde("Neptune");
    Objet o1 = new Objet("hache", m){ public boolean estDeplacable(){return true;}};
    Objet o2 = new Objet("hache", m){ public boolean estDeplacable(){return true;}};
  }
}
