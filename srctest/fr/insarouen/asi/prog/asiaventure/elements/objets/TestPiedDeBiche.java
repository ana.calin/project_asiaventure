
package fr.insarouen.asi.prog.asiaventure.elements.objets;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
* Création de la classe TestPiedDeBiche
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestPiedDeBiche {

  @Test
  public void estDeplacableTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Monde m = new Monde("Terre");
    PiedDeBiche crowbar = new PiedDeBiche("crowbar", m);
    assertThat(crowbar.estDeplacable(), is(true));
  }

  @Test(expected=NomDEntiteDejaUtiliseDansLeMondeException.class)
  public void testExceptionConstructeurObjet() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Monde m = new Monde("Neptune");
    PiedDeBiche crowbar1 = new PiedDeBiche("crowbar", m);
    PiedDeBiche crowbar2 = new PiedDeBiche("crowbar", m);
  }
}
