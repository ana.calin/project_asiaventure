// Générer le nom de la clé aléatoirement dans créerclé en vérifiant qu'il n'existe pas à cause d'un coffre fort
// Générer le nom de la serrure aléatoirement dans le constructeur sans nom en vérifiant qu'il n'existe pas

package fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.Activable;
import fr.insarouen.asi.prog.asiaventure.elements.Etat;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleAvecObjetException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
* Création de la classe TestObjet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestSerrure {
  private Monde monde;
  private Clef clef;
  private Serrure serrure;

  @Before
  public void avantTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
    this.monde = new Monde("Terre");
    this.serrure = new Serrure("Serrure", this.monde);
    this.clef = this.serrure.creerClef();
  }

  @Test
  public void Test_constructeurSerrure1() throws NomDEntiteDejaUtiliseDansLeMondeException {
    Serrure serrure2 = new Serrure(this.monde);
    assertThat(serrure2.getMonde().getNom().equals("Terre"), is(true));
    assertThat(serrure2.getNom().equals("serrure1"), is(true));
  }

  @Test
  public void Test_constructeurSerrure2() throws NomDEntiteDejaUtiliseDansLeMondeException {
    assertThat(this.serrure.getNom().equals("Serrure"), is(true));
    assertThat(this.serrure.getMonde().getNom().equals("Terre"), is(true));
  }

  @Test
  public void Test_creerClef() {
    assertThat(this.clef != null, is(true));
  }

  @Test
  public void Test_getEtat() {
    assertThat(this.serrure.getEtat() == Etat.DEVERROUILLE, is(true));
  }

  @Test
  public void Test_activerAvec() throws ActivationImpossibleAvecObjetException {

    assertThat(this.serrure.getEtat() == Etat.DEVERROUILLE, is(true));

    this.serrure.activerAvec(this.clef);;
    assertThat(this.serrure.getEtat() == Etat.VERROUILLE, is(true));
  }

  // Test exception
  @Test(expected = ActivationImpossibleAvecObjetException.class)
  public void Test_activerAvec_exception() throws ActivationImpossibleAvecObjetException, NomDEntiteDejaUtiliseDansLeMondeException {
    Objet objetAuHasard = new Objet("nomObjet", this.monde){public boolean estDeplacable() {return false;}};

    assertThat(this.serrure.getEtat() == Etat.DEVERROUILLE, is(true));

    this.serrure.activerAvec(objetAuHasard); // Provoque ActivationImpossibleAvecObjetException
  }

  @Test
  public void Test_estDeplacable() {
    assertThat(this.serrure.estDeplacable(), is(false));
  }

  @Test
  public void Test_activableAvec() throws NomDEntiteDejaUtiliseDansLeMondeException {
    assertThat(this.serrure.activableAvec(this.clef), is(true));

    Objet objetAuHasard = new Objet("nomObjet", this.monde){public boolean estDeplacable() {return false;}};
    assertThat(this.serrure.activableAvec(objetAuHasard), is(false));

    Clef clef2= new Clef("Clef2", this.monde);
    assertThat(this.serrure.activableAvec(clef2), is(false));
  }

  // Test exception
  @Test(expected = ActivationImpossibleException.class)
  public void Test_activer() throws ActivationImpossibleException {
    this.serrure.activer();
  }
}
