package fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
* Création de la classe TestObjet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/

@RunWith(Suite.class)
@SuiteClasses({
  TestClef.class,
  TestSerrure.class
})
public class SuiteTestSerrurerie {}
