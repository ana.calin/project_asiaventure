package fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie;

import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

// Import static
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
* Création de la classe TestObjet
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestClef {
  private Monde monde;
  private Clef cle;

  @Before
  public void avantTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
    this.monde = new Monde("Terre");
    this.cle = new Clef("Clef", this.monde);
  }

  @Test
  public void Test_constructeurClef() {
    assertThat(this.cle.getNom().equals("Clef"), is(true));
    assertThat(this.cle.getMonde().getNom().equals("Terre"), is(true));
  }

  @Test
  public void Test_estDeplacable() {
    assertThat(this.cle.estDeplacable(), is(true));
  }
}
