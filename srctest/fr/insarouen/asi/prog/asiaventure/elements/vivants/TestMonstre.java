package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import fr.insarouen.asi.prog.asiaventure.*;
import fr.insarouen.asi.prog.asiaventure.elements.*;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.structure.*;

import java.util.*;

/**
* Création de la classe TestMonstre
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestMonstre {
     Piece piece, piece2, piece3, piece4, piece5, piece6;
     Porte porte,porte2,porte3,porte4,porte5,porte6;
     Monstre monstre;
     Objet objet, objet2, objet3, objet4, objet5, objet6;
     Objet[] objets;
     Monde mondejava;
@Before
  public void avantTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
    this.mondejava=new Monde("mondejava");
    this.piece = new Piece("piece",mondejava);
    this.objet = new PiedDeBiche("PiedDeBiche",mondejava);
    this.objet2 = new PiedDeBiche("PiedDeBiche2",mondejava);
    this.objet3 = new PiedDeBiche("PiedDeBiche3",mondejava);
    this.objet4 = new Coffre("Coffre1",mondejava);
    this.objet5 = new Objet("Pioche",mondejava){public boolean estDeplacable(){ return false;}};
    this.objet6 = new Objet("Marteau",mondejava){public boolean estDeplacable(){ return true;}};
    Objet[] objets = {objet,objet4};
    this.monstre=new Monstre("monstre",mondejava,2,6,piece,objets);

    this.piece2 = new Piece("piece2",mondejava);
    this.piece3 = new Piece("piece3",mondejava);
    this.piece4 = new Piece("piece4",mondejava);
    this.piece5 = new Piece("piece5",mondejava);
    this.piece6 = new Piece("piece6",mondejava);

    this.porte = new Porte("porte", mondejava, piece, piece2);
    this.porte2 = new Porte("porte2", mondejava, piece2, piece3);
    this.porte3 = new Porte("porte3", mondejava, piece3, piece);
    this.porte4 = new Porte("porte4", mondejava, piece2, piece4);
    this.porte5 = new Porte("porte5", mondejava, piece, piece6);
    this.porte6 = new Porte("porte6", mondejava, piece6, piece5);

    this.piece.deposer(this.objet);
    this.piece2.deposer(this.objet2);
    this.piece3.deposer(this.objet3);
    this.piece.deposer(this.objet4);
    this.piece5.deposer(this.objet5);
    this.piece6.deposer(this.objet6);

    this.piece.addPorte(this.porte);
    this.piece.addPorte(this.porte3);
    this.piece.addPorte(this.porte5);
    this.piece2.addPorte(this.porte);
    this.piece2.addPorte(this.porte2);
    this.piece2.addPorte(this.porte4);
    this.piece3.addPorte(this.porte2);
    this.piece3.addPorte(this.porte3);
    this.piece4.addPorte(this.porte4);
    this.piece5.addPorte(this.porte6);
    this.piece6.addPorte(this.porte6);
    this.piece6.addPorte(this.porte5);
  }

  @Test
  public void testConstructeur()  throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
    assertThat(this.monstre.getPointVie(), is(2));
    assertThat(this.monstre.getPointForce(), is(6));
    assertThat(this.monstre.getPiece().equals(this.piece), is(true));
    assertThat(this.monstre.getMonde().getNom().equals(this.mondejava.getNom()), is(true));

  }

// @Test
//   public void testAttraperEtDeposerObjets() throws ObjetNonPossedeParLeVivantException, ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
//     assertThat(this.monstre.possede(objet2), is(true));
//     assertThat(this.monstre.possede(objet3), is(true));
//     assertThat(this.piece.contientObjet(objet), is(true));
//     assertThat(this.piece.contientObjet(objet4), is(true));
//     monstre.attraperEtDeposerObjets();
//     assertThat(this.monstre.possede(objet2), is(false));
//     assertThat(this.monstre.possede(objet3), is(false));
//     assertThat(this.monstre.possede(objet), is(true));
//     assertThat(this.piece.contientObjet(objet4), is(true));
//     assertThat(this.piece.contientObjet(objet), is(false));
//     assertThat(this.piece.contientObjet(objet2), is(true));
//     assertThat(this.piece.contientObjet(objet3), is(true));
//   }
@Test
  public void testExecuter() throws NomDEntiteDejaUtiliseDansLeMondeException,ActivationImpossibleException,PorteFermeException, PorteInexistanteDansLaPieceException,ObjetNonPossedeParLeVivantException, ObjetNonDeplacableException, ObjetAbsentDeLaPieceException {
    assertThat(this.monstre.getPointVie(), is(2));
    this.monstre.executer();
    assertThat(this.monstre.getPointVie(), is(1));
    if (this.monstre.getPiece()==piece2){
      assertThat(this.piece2.contientObjet(objet), is(true));
      assertThat(this.piece2.contientObjet(objet2), is(false));
      assertThat(this.monstre.possede(objet2), is(true));
      assertThat(this.monstre.possede(objet), is(false));
    }
    if (this.monstre.getPiece()==piece3){
      assertThat(this.piece3.contientObjet(objet), is(true));
      assertThat(this.piece3.contientObjet(objet3), is(false));
      assertThat(this.monstre.possede(objet3), is(true));
      assertThat(this.monstre.possede(objet), is(false));
    }
    if (this.monstre.getPiece()==piece6){
      assertThat(this.piece6.contientObjet(objet), is(true));
      assertThat(this.piece6.contientObjet(objet6), is(false));
      assertThat(this.monstre.possede(objet6), is(true));
      assertThat(this.monstre.possede(objet), is(false));
    }
}

}
