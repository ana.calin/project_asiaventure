package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import java.lang.reflect.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import fr.insarouen.asi.prog.asiaventure.elements.vivants.CommandeImpossiblePourLeVivantException;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Porte;
import fr.insarouen.asi.prog.asiaventure.elements.objets.ObjetNonDeplacableException;
import  fr.insarouen.asi.prog.asiaventure.elements.structure.VivantAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteInexistanteDansLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteFermeException;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.ObjetNonPossedeParLeVivantException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationException;
import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.objets.Objet;
import fr.insarouen.asi.prog.asiaventure.elements.Etat;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Serrure;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;

import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;

import java.util.*;

/**
* Création de la classe TestJoueurHumain
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestJoueurHumain {

   private Piece piece, piece2, piece3;
   private JoueurHumain human;
   private Monde mondejava;
   private Objet objet, objet2, objet3;

   @Before
   public void avantTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
     this.mondejava=new Monde("mondejava");
     this.piece = new Piece("piece",this.mondejava);
     this.objet = new PiedDeBiche("PiedDeBiche",this.mondejava);
     this.objet2 = new PiedDeBiche("PiedDeBiche2",this.mondejava);
     this.objet3 = new PiedDeBiche("PiedDeBiche3",this.mondejava);
     Objet[] objets = new Objet[2];
     objets[0]=this.objet2;
     objets[1]=this.objet3;
     this.human=new JoueurHumain("JoueurHumain",this.mondejava,10,15,this.piece,objets){};
   }

  @Test
  public void Test_executer() throws CommandeImpossiblePourLeVivantException, Throwable {
    PiedDeBiche crowbar= new PiedDeBiche("crowbar",this.mondejava);

    // commandePrendre
    this.piece.deposer(crowbar);
    this.human.setOrdre("Prendre crowbar");

    assertThat(this.piece.contientVivant(this.human), is(true));
    assertThat(this.piece.contientObjet(crowbar), is(true));

    this.human.executer();
    assertThat(this.human.possede(crowbar), is(true));
    assertThat(this.piece.contientObjet(crowbar), is(false));

    // commandePoser
    this.human.setOrdre("Poser crowbar");
    this.human.executer();
    assertThat(this.human.possede(crowbar), is(false));
    assertThat(this.piece.contientObjet(crowbar), is(true));

    // commandeFranchir
    Piece piece2 = new Piece("piece2",this.mondejava);
    Porte porteSansSerrure = new Porte("porteSansSerrure", this.mondejava, this.piece, piece2);
    this.human.setOrdre("Franchir porteSansSerrure");
    this.human.executer();
    assertThat(this.piece.contientVivant(this.human), is(false));
    assertThat(piece2.contientVivant(this.human), is(true));

    // commandeOuvrirPorte (avec un seul paramètre)
    porteSansSerrure.activer();
    assertThat(porteSansSerrure.getEtat()==Etat.FERME, is(true));
    this.human.setOrdre("OuvrirPorte porteSansSerrure");
    this.human.executer();
    assertThat(porteSansSerrure.getEtat()==Etat.OUVERT, is(true));

    // commandeOuvrirPorte (avec deux paramètres)
    Serrure serrure = new Serrure("Serrure", this.mondejava);
    Clef clef = serrure.creerClef();
    Porte porteAvecSerrure = new Porte("porteAvecSerrure", this.mondejava, serrure, this.piece, piece2);
    this.human.setOrdre("OuvrirPorte porteAvecSerrure "+clef.getNom());
    piece2.deposer(clef);
    this.human.prendre(clef);
    this.human.executer();
    assertThat(porteAvecSerrure.getEtat()==Etat.VERROUILLE, is(true));
  }

  @Test(expected=CommandeImpossiblePourLeVivantException.class)
  public void Test_executer_AvecException() throws CommandeImpossiblePourLeVivantException, Throwable {
    PiedDeBiche crowbar= new PiedDeBiche("crowbar",this.mondejava);
    this.piece.deposer(crowbar);
    this.human.setOrdre("Prendre crowbar crowbar"); // On donne exprès trop de paramamètres pour cette commande

    this.human.executer();

  }

  @Test(expected=ObjetAbsentDeLaPieceException.class)
  public void Test_executer_AvecException2() throws CommandeImpossiblePourLeVivantException, Throwable {
    PiedDeBiche crowbar= new PiedDeBiche("crowbar",this.mondejava);
    this.piece.deposer(crowbar);
    this.human.setOrdre("Prendre cr"); // On donne exprès trop de paramamètres pour cette commande

    this.human.executer();
  }

  @Test
  public void Test_setOrdre() {
    human.setOrdre("Ouvrir Porte"); // On n'a pas de getter pour l'attribut de JoueurHumain
    String chaine="Ouvrir Porte";
    assertThat(chaine.equals("Ouvrir Porte"), is(true));
  }

}
