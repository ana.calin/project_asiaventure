package fr.insarouen.asi.prog.asiaventure.elements.vivants;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Piece;
import fr.insarouen.asi.prog.asiaventure.elements.objets.*;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.Vivant;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;
import  fr.insarouen.asi.prog.asiaventure.elements.structure.ObjetAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.ObjetNonPossedeParLeVivantException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteFermeException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.VivantAbsentDeLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.PorteInexistanteDansLaPieceException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationException;
import fr.insarouen.asi.prog.asiaventure.elements.structure.Porte;
import fr.insarouen.asi.prog.asiaventure.elements.Etat;

import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleAvecObjetException;
import fr.insarouen.asi.prog.asiaventure.elements.ActivationImpossibleException;
import fr.insarouen.asi.prog.asiaventure.elements.Activable;
import fr.insarouen.asi.prog.asiaventure.elements.objets.PiedDeBiche;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Serrure;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.Clef;

import java.util.*;

/**
* Création de la classe TestVivant
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestVivant {

   private Piece piece, piece2, piece3;
   private Vivant vivant;
   private Monde mondejava;
   private Objet objet, objet2, objet3;

   @Before
   public void avantTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
     this.mondejava=new Monde("mondejava");
     this.piece = new Piece("piece",this.mondejava);
     this.objet = new PiedDeBiche("PiedDeBiche",this.mondejava);
     this.objet2 = new PiedDeBiche("PiedDeBiche2",this.mondejava);
     this.objet3 = new PiedDeBiche("PiedDeBiche3",this.mondejava);
     Objet[] objets = new Objet[2];
     objets[0]=this.objet2;
     objets[1]=this.objet3;
     this.vivant=new Vivant("vivant",this.mondejava,10,15,this.piece,objets){};
     this.piece.deposer(this.objet);
     this.piece2 = new Piece("piece2",this.mondejava);
     this.piece3 = new Piece("piece3",this.mondejava);
   }

  @Test
  public void testConstructeur()  throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException {
    assertThat(this.vivant.getPointVie(), is(10));
    assertThat(this.vivant.getPointForce(), is(15));
    assertThat(this.vivant.getPiece().equals(this.piece), is(true));
    assertThat(this.vivant.getMonde().getNom().equals(this.mondejava.getNom()), is(true));
  }

  @Test
  public void testFranchir() throws NomDEntiteDejaUtiliseDansLeMondeException, PorteFermeException, PorteInexistanteDansLaPieceException, ActivationException {
  	Vivant v = new Vivant("Jamie", this.mondejava, 40, 6, this.piece2, this.objet2, this.objet3) {};
  	Porte porte = new Porte("porte", this.mondejava, this.piece2, this.piece3); // Une porte est ouverte par défaut

    this.piece2.addPorte(porte);
    this.piece3.addPorte(porte);
  	assertThat(porte.getEtat() == Etat.OUVERT, is(true));
    assertThat(this.piece2.aLaPorte(porte), is(true));
    assertThat(this.piece3.aLaPorte(porte), is(true));

    //this.piece2.entrer(v);
  	assertThat(this.piece2.contientVivant(v), is(true));
    assertThat(this.piece3.contientVivant(v), is(false));

    // Changement de Piece : piece2 à piece3
    v.franchir("porte");
    assertThat(this.piece2.contientVivant(v), is(false));
  	assertThat(this.piece3.contientVivant(v), is(true));

    // Changement de Piece : piece3 à piece2
  	v.franchir(porte);
		assertThat(this.piece2.contientVivant(v), is(true));
		assertThat(this.piece3.contientVivant(v), is(false));
  }

  // Test testFranchir PorteInexistanteDansLaPieceException
  @Test(expected=PorteInexistanteDansLaPieceException.class)
  public void testFranchir_exception() throws NomDEntiteDejaUtiliseDansLeMondeException, PorteFermeException, PorteInexistanteDansLaPieceException, ActivationException {
  	Vivant v = new Vivant("Jamie", this.mondejava, 40, 6, this.piece, this.objet2, this.objet3) {};
  	Porte porte = new Porte("porte", this.mondejava, this.piece2, this.piece3); // Une porte est ouverte par défaut

    this.piece2.addPorte(porte);
    this.piece3.addPorte(porte);
    assertThat(porte.getEtat() == Etat.OUVERT, is(true));
    assertThat(this.piece2.aLaPorte(porte), is(true));
    assertThat(this.piece3.aLaPorte(porte), is(true));

    //this.piece.entrer(v);
    assertThat(this.piece.contientVivant(v), is(true));
  	assertThat(this.piece2.contientVivant(v), is(false));
    assertThat(this.piece3.contientVivant(v), is(false));

    v.franchir("porte");
  }

  @Test
  public void test_prendre() throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException, ObjetNonPossedeParLeVivantException {
    this.vivant.prendre(this.objet);
    assertThat(vivant.possede(this.objet), is(true));
    this.vivant.deposer(this.objet);
  }

  @Test
  public void test_deposer() throws ObjetAbsentDeLaPieceException, ObjetNonDeplacableException, ObjetNonPossedeParLeVivantException {
    this.vivant.deposer(this.objet2);
    assertThat(vivant.possede(this.objet2), is(false));
    assertThat(piece.contientObjet(objet2), is(true));
    this.vivant.prendre(this.objet2);
  }

  @Test
  public void test_getObjet(){
    assertThat(vivant.getObjet("PiedDeBiche2"), is(objet2));
  }

  // Test getObjets()
  @Test
  public void Test_getObjets() {
    Map<String, Objet> mapInventaireVivant = this.vivant.getObjets();

    Objet[] objets = new Objet[2];
    objets[0]=this.objet2;
    objets[1]=this.objet3;

    for (int i = 0; i<objets.length; i++) {
      assertThat(mapInventaireVivant.containsValue(objets[i]), is(true));
    }
  }

  // Test activerActivable
  @Test(expected = ActivationImpossibleException.class)
  public void Test_activerActivable() throws ActivationException, NomDEntiteDejaUtiliseDansLeMondeException {
    Serrure serrure = new Serrure(this.mondejava);
    Porte porte = new Porte("porte1", this.mondejava, serrure, this.piece, this.piece2);


    assertThat(porte.getEtat()==Etat.OUVERT, is(true));

    this.vivant.activerActivable(porte);
    assertThat(porte.getEtat()==Etat.FERME, is(true));

    this.vivant.activerActivable(serrure);
  }

  // Test activerActivableAvecObjet
  @Test
  public void Test_activerActivableAvecObjet() throws ActivationException, NomDEntiteDejaUtiliseDansLeMondeException {
    Serrure serrure = new Serrure(this.mondejava);
    Clef clef = serrure.creerClef();
    Porte porte = new Porte("porte1", this.mondejava, serrure, this.piece, this.piece2);

    assertThat(porte.getEtat()==Etat.OUVERT, is(true));
    assertThat(serrure.getEtat()==Etat.DEVERROUILLE, is(true));

    this.vivant.activerActivableAvecObjet(porte, clef);
    assertThat(porte.getEtat()==Etat.VERROUILLE, is(true));

    this.vivant.activerActivableAvecObjet(serrure, clef);
    assertThat(serrure.getEtat()==Etat.VERROUILLE, is(true));

    PiedDeBiche piedDeBiche = new PiedDeBiche("crowbar", this.mondejava);
    this.vivant.activerActivableAvecObjet(porte, piedDeBiche);
    assertThat(porte.getEtat()==Etat.CASSE, is(true));
  }
}
