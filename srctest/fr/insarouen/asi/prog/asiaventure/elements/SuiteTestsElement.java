package fr.insarouen.asi.prog.asiaventure.elements;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.insarouen.asi.prog.asiaventure.elements.TestEntite;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.TestVivant;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.TestMonstre;
import fr.insarouen.asi.prog.asiaventure.elements.vivants.TestJoueurHumain;

/**
* Création SuiteTestsElement
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
@RunWith(Suite.class)
@SuiteClasses({
    TestEntite.class,
    TestVivant.class,
    TestMonstre.class,
    TestJoueurHumain.class
})
public class SuiteTestsElement{}
