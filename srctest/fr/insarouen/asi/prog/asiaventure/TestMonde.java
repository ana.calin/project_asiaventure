package fr.insarouen.asi.prog.asiaventure;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import java.awt.Color;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import fr.insarouen.asi.prog.asiaventure.Monde;
import fr.insarouen.asi.prog.asiaventure.elements.Entite;
import fr.insarouen.asi.prog.asiaventure.NomDEntiteDejaUtiliseDansLeMondeException;

/**
* Création de la classe TestMonde
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
public class TestMonde {

    public Monde mondeJava1, mondeJava2;
    public Entite entite2;
@Before
      public void avantTest() throws NomDEntiteDejaUtiliseDansLeMondeException {
        mondeJava1 = new Monde("mondeJava1");
        mondeJava2 = new Monde("mondeJava2");
        entite2 = new Entite("nomDeLEntite2", mondeJava2){};
      }


@Test
      public void test_constructeur() {
        assertThat(mondeJava1.getNom(), is("mondeJava1"));
      }

@Test
      public void test_ajouter() throws NomDEntiteDejaUtiliseDansLeMondeException {
        assertThat(mondeJava2.getEntite("nomDeLEntite2"), is(entite2));
      }

@Test(expected=NomDEntiteDejaUtiliseDansLeMondeException.class)
      public void test_ExceptionNomDEntiteDejaUtiliseDansLeMondeAjouter() throws NomDEntiteDejaUtiliseDansLeMondeException {
        Entite entite1 = new Entite("nomDeLEntite2", mondeJava2){};
      }

@Test(expected=EntiteDejaDansUnAutreMondeException.class)
      public void test_ExceptionEntiteDejaDansUnAutreMondeAjouter() throws NomDEntiteDejaUtiliseDansLeMondeException, EntiteDejaDansUnAutreMondeException {
        mondeJava1.ajouter(entite2);
      }



}
