package fr;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.insarouen.asi.prog.asiaventure.elements.SuiteTestsElement;
import fr.insarouen.asi.prog.asiaventure.TestMonde;
import fr.insarouen.asi.prog.asiaventure.TestSimulateur;
import fr.insarouen.asi.prog.asiaventure.elements.objets.SuiteTestsObjets;
import fr.insarouen.asi.prog.asiaventure.elements.structure.SuiteTestsStructure;
import fr.insarouen.asi.prog.asiaventure.elements.objets.serrurerie.SuiteTestSerrurerie;

/**
* Création AllTests
* @author CALIN Ana, TOOMEY Damien
* @version 1.0
*/
@RunWith(Suite.class)
@SuiteClasses({
    SuiteTestsElement.class,
    TestMonde.class,
    SuiteTestsObjets.class,
    SuiteTestsStructure.class,
    SuiteTestSerrurerie.class,
    TestSimulateur.class
})
public class AllTests{}
