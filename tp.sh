# Suppression des .class dans src
find classes/ -name *.class | xargs rm -f

# Suppression des .class dans src
find classestest/ -name *.class | xargs rm -f

# Suppression de la documentation
rm -fr ./doc/*

# Compilation src
find src -name *.java | xargs javac -classpath ./classes -sourcepath ./src -d ./classes -Xlint:unchecked

# Compilation srctest
find srctest/ -name *.java | xargs javac -classpath ./classestest:./classes/:/usr/share/java/junit4.jar:/usr/share/java/hamcrest-library.jar -sourcepath ./srctest -d ./classestest

# Execution tous les Tests
#java -classpath ./classestest:./classes/:/usr/share/java/junit4.jar:/usr/share/java/hamcrest-library.jar org.junit.runner.JUnitCore fr.AllTests

# Execution Main
java -classpath classes fr.insarouen.asi.prog.asiaventure.Main

# Documentation
#javadoc -d ./doc -encoding UTF-8 -docencoding UTF-8 -sourcepath ./src -subpackages . -charset UTF-8
